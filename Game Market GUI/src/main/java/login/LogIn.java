package login;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class LogIn extends JDialog{

	private JTextField username;
	private JPasswordField password;
	private JLabel lUsername;
	private JLabel lPassword;
	private JButton login;
	private JButton cancel;
	private boolean flag;
	
	public LogIn(JFrame frame) {
		super(frame, "Login", true);
		
		JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints constraint = new GridBagConstraints();
		
		lUsername = new JLabel("Username: ");
		constraint.gridx = 0;
		constraint.gridy = 0;
		constraint.gridwidth = 1;
		panel.add(lUsername, constraint);
		
		username = new JTextField(20);
		constraint.gridx = 1;
		constraint.gridy = 0;
		constraint.gridwidth = 2;
        panel.add(username, constraint);
 
        lPassword = new JLabel("Password: ");
        constraint.gridx = 0;
        constraint.gridy = 1;
        constraint.gridwidth = 1;
        panel.add(lPassword, constraint);
 
        password = new JPasswordField(20);
        constraint.gridx = 1;
        constraint.gridy = 1;
        constraint.gridwidth = 2;
        panel.add(password, constraint);
        panel.setBorder(new LineBorder(Color.BLACK));
        
        JPanel account = new JPanel();
        JLabel create = new JLabel("Create Account");
        create.addMouseListener(new MouseAdapter() {
        	public void mouseClicked() {
        		
        	}
        });
        JLabel forgot = new JLabel("Forgot Password?");
        forgot.addMouseListener(new MouseAdapter() {
        	public void mouseClickced() {
        		
        	}
        });
        
        login = new JButton("Login");
        
        login.addActionListener(new ActionListener() {
        	
        	public void actionPerformed(ActionEvent e) {
        		
        	}
        });
        
        cancel = new JButton("Cancel");
        cancel.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		dispose();
        	}
        });
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(login);
        buttonPanel.add(cancel);
        
        getContentPane().add(panel, BorderLayout.PAGE_START);
        getContentPane().add(account, BorderLayout.CENTER);
        getContentPane().add(buttonPanel, BorderLayout.PAGE_END);
        
        pack();
        setResizable(false);
        setLocationRelativeTo(frame);
		
	}
}
