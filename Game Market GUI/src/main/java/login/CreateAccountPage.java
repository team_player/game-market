package login;

import java.awt.Color;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import actors.*;
import mainPage.MainPage;

public class CreateAccountPage {
	
	private JFrame frame;
	
	private JTextField firstNameField;
	private JTextField lastNameField;
	private JTextField usernameField;
	private JTextField emailField;
	private JPasswordField passwordField;
	private JPasswordField passwordVerifField;
	
	private final Color WHITE = new Color(255, 255, 255);

	public CreateAccountPage() {
		createFrame();
		createFields();
		createText();
		createButton();
	}
	
	private void createFrame() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(142, 93, 27));
		frame.setBounds(100, 100, 550, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
	}
	
	private void createFields() {
		// first name text field
		firstNameField = new JTextField();
		firstNameField.setFont(new Font("Arial", Font.PLAIN, 18));
		firstNameField.setBounds(208, 90, 270, 29);
		frame.getContentPane().add(firstNameField);
		
		// last name text field
		lastNameField = new JTextField();
		lastNameField.setFont(new Font("Arial", Font.PLAIN, 18));
		lastNameField.setBounds(208, 129, 270, 29);
		frame.getContentPane().add(lastNameField);
		
		// username text field
		usernameField = new JTextField();
		usernameField.setFont(new Font("Arial", Font.PLAIN, 18));
		usernameField.setBounds(208, 168, 270, 29);
		frame.getContentPane().add(usernameField);
		
		// email address text field
		emailField = new JTextField();
		emailField.setFont(new Font("Arial", Font.PLAIN, 18));
		emailField.setBounds(208, 207, 270, 29);
		frame.getContentPane().add(emailField);
		
		// password text field
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Arial", Font.PLAIN, 25));
		passwordField.setBounds(208, 246, 270, 29);
		frame.getContentPane().add(passwordField);
		
		// password verification field
		passwordVerifField = new JPasswordField();
		passwordVerifField.setFont(new Font("Arial", Font.PLAIN, 25));
		passwordVerifField.setBounds(208, 285, 270, 29);
		frame.getContentPane().add(passwordVerifField);
	}
	
	private void createText() {
		// Create Account title
		JLabel createAccount = new JLabel("Create Account");
		createAccount.setBounds(191, 35, 180, 20);
		createAccount.setFont(new Font("Britannic Bold", Font.PLAIN, 27));
		createAccount.setForeground(WHITE);
		frame.getContentPane().add(createAccount);
		
		// First name label
		JLabel firstNameLabel = new JLabel("First Name:");
		firstNameLabel.setBounds(50, 90, 200, 29);
		firstNameLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		firstNameLabel.setForeground(WHITE);
		frame.getContentPane().add(firstNameLabel);
		
		// Last name label
		JLabel lastNameLabel = new JLabel("Last Name:");
		lastNameLabel.setBounds(50, 129, 200, 29);
		lastNameLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		lastNameLabel.setForeground(WHITE);
		frame.getContentPane().add(lastNameLabel);
		
		// username label
		JLabel usernameLabel = new JLabel("username:");
		usernameLabel.setBounds(50, 168, 200, 29);
		usernameLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		usernameLabel.setForeground(WHITE);
		frame.getContentPane().add(usernameLabel);
		
		// email address label
		JLabel emailLabel = new JLabel("email address:");
		emailLabel.setBounds(50, 207, 200, 29);
		emailLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		emailLabel.setForeground(WHITE);
		frame.getContentPane().add(emailLabel);
		
		// password label
		JLabel passwordLabel = new JLabel("password:");
		passwordLabel.setBounds(50, 246, 200, 29);
		passwordLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		passwordLabel.setForeground(WHITE);
		frame.getContentPane().add(passwordLabel);

		// password verification label
		JLabel passwordVerifLabel = new JLabel("verify password:");
		passwordVerifLabel.setBounds(50, 285, 200, 29);
		passwordVerifLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		passwordVerifLabel.setForeground(WHITE);
		frame.getContentPane().add(passwordVerifLabel);

		// already have account label
		JLabel haveAccountLabel = new JLabel("Already have an account?");
		haveAccountLabel.setBounds(177, 379, 200, 15);
		haveAccountLabel.setFont(new Font("Arial", Font.ITALIC, 10));
		haveAccountLabel.setForeground(WHITE);
		frame.getContentPane().add(haveAccountLabel);
	}
	
	private void createButton() {
		// submit button
		JButton btnCreateAccount = new JButton("submit");
		btnCreateAccount.setBounds(230, 335, 90, 29);
		btnCreateAccount.setForeground(new Color(0, 0, 0));
		btnCreateAccount.setFont(new Font("Britannic Bold", Font.PLAIN, 18));
		frame.getContentPane().add(btnCreateAccount);
		btnCreateAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (passwordField.getText().equals(passwordVerifField.getText())) {
					frame.dispose();
					User actor = new User(firstNameField.getText(), lastNameField.getText(), 
							emailField.getText(), passwordField.getText(), usernameField.getText());
					actor.DisplayUser();
					MainPage mainPage = new MainPage(actor);
					mainPage.getFrame().setVisible(true);
				}
			}
		});
		
		// login button
		JButton btnLogin = new JButton("Sign in");
		btnLogin.setBounds(307, 379, 66, 15);
		btnLogin.setForeground(new Color(0, 0, 0));
		btnLogin.setFont(new Font("Arial", Font.ITALIC, 10));
		frame.getContentPane().add(btnLogin);
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				LoginPage login = new LoginPage();
				login.getFrame().setVisible(true);
			}
		});
	}
	
	public Window getFrame() {
		return frame;
	}

}
