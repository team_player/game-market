package login;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import actors.User;
import mainPage.MainPage;
import net.miginfocom.swing.MigLayout;

public class LoginPage {

	private JFrame frame;
	
	private JButton btnLogin;
	private JButton btnCreateAccount;
	private JButton btnForgotPassword;

	private JTextField usernameField;
	private JPasswordField passwordField;
	
	public LoginPage() {
		createFrame();
		createLabels();
		createButtons();
		createTextFields();
	}
	
	private void createFrame() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(142, 93, 27));
		frame.setBounds(200, 120, 550, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
	}

	private void createLabels() {
		// username label
		JLabel lblUsername = new JLabel("USERNAME");
		lblUsername.setBounds(100, 97, 200, 17);
		lblUsername.setFont(new Font("Britannic Bold", Font.PLAIN, 21));
		lblUsername.setForeground(new Color(255, 255, 255));
		frame.getContentPane().add(lblUsername);
		
		// password label
		JLabel lblPassword = new JLabel("PASSWORD");
		lblPassword.setBounds(100, 163, 200, 17);
		lblPassword.setFont(new Font("Britannic Bold", Font.PLAIN, 21));
		lblPassword.setForeground(new Color(255, 255, 255));
		frame.getContentPane().add(lblPassword);
	}
	
	private void createButtons() {
		// login button
		btnLogin = new JButton("Log-in");
		btnLogin.setBounds(100, 228, 83, 26);
		btnLogin.setForeground(new Color(0, 0, 0));
		btnLogin.setFont(new Font("Britannic Bold", Font.PLAIN, 18));
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				User user = new User("","","","","");		// later will be actual user
				frame.dispose();
				MainPage mainPage = new MainPage(user);
				mainPage.getFrame().setVisible(true);
			}
		});
		frame.getContentPane().add(btnLogin);
		
		// create account button
		btnCreateAccount = new JButton("Create Account");
		btnCreateAccount.setBounds(100, 266, 156, 26);
		btnCreateAccount.setForeground(new Color(0, 0, 0));
		btnCreateAccount.setFont(new Font("Britannic Bold", Font.PLAIN, 18));
		btnCreateAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				CreateAccountPage createAccount = new CreateAccountPage();
				createAccount.getFrame().setVisible(true);
			}
		});
		frame.getContentPane().add(btnCreateAccount);
	
		// forgot password button
		btnForgotPassword = new JButton("forgot password?");
		btnForgotPassword.setBounds(334, 163, 116, 17);
		btnForgotPassword.setFont(new Font("Lucida Calligraphy", Font.PLAIN, 9));
		btnForgotPassword.setForeground(new Color(0, 0, 0));
		frame.getContentPane().add(btnForgotPassword);
	}
	
	private void createTextFields() {
		// username field
		usernameField = new JTextField();
		usernameField.setBounds(100, 119, 350, 29);
		usernameField.setFont(new Font("Arial", Font.PLAIN, 18));
		frame.getContentPane().add(usernameField);
		
		// password field
		passwordField = new JPasswordField();
		passwordField.setBounds(100, 185, 350, 29);
		passwordField.setFont(new Font("Arial", Font.PLAIN, 25));
		frame.getContentPane().add(passwordField);
	}
	
	public JFrame getFrame() {
		return frame;
	}
	
}
