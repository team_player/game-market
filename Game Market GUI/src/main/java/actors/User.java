package actors;

public class User {
	
	private Account account;
	private String username;
	private Boolean frozen;
	private Boolean loggedin;
	
	public User(String firstName, String lastName, String email, 
				String password, String username) {
		this.account = new Account(firstName, lastName, email, password);
		this.username = username;
		this.frozen = false;
		this.loggedin = true;
	}
	
	public void DisplayUser() {
		System.out.println(account.getFirstName() + " " + account.getLastName() + " created account number "
				+ account.getUserID() + " on " + account.getDateJoined() + " under the username " 
				+ this.username + " with the email address " + account.getEmail());
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Account getAccount() {
		return account;
	}

	public Boolean getFrozen() {
		return frozen;
	}

	public Boolean getLoggedin() {
		return loggedin;
	}
	
}
