package actors;

import java.sql.Timestamp;
import java.util.Date;

public class Account {

	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String bio;
	private Integer userID;
	private Timestamp dateJoined;
	
	private static Integer numAccounts = 0;
	
	public Account(String firstName, String lastName, String email, 
					String password) {
		numAccounts += 1;
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.bio = "";
		this.userID = numAccounts;
		this.dateJoined = new Timestamp(System.currentTimeMillis());
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getBio() {
		return bio;
	}
	
	public void setBio(String bio) {
		this.bio = bio;
	}
	
	public Integer getUserID() {
		return userID;
	}

	public Timestamp getDateJoined() {
		return dateJoined;
	}

	public static Integer getNumAccounts() {
		return numAccounts;
	}

}
