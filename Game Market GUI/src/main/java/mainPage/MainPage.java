package mainPage;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

import actors.User;
import login.LoginPage;
import page.*;

public class MainPage {
	
	private JFrame frame;

	private JPanel panel1 = null;
	private JPanel panel2 = null;
	private String currentPanel = null;
	
	private User account;

	public MainPage(User user) {
		account = user;
		createFrame();
		createButtons();
	}
	
	private void createFrame() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(13, 20, 36));
		frame.setBounds(100, 20, 750, 650);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
	}
	
	private void createButtons() {
		final JButton btnBuysell = new JButton("Buy/Sell");
		btnBuysell.setBackground(new Color(142, 93, 27));
		btnBuysell.setForeground(new Color(255, 255, 255));
		btnBuysell.setFont(new Font("Baskerville Old Face", Font.PLAIN, 15));
		btnBuysell.setBounds(0, 0, 85, 28);
		frame.getContentPane().add(btnBuysell);
		btnBuysell.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (currentPanel != "Buy/Sell") {
					if (panel1 != null) {
						frame.remove(panel1);
					}
					if (panel2 != null) {
						frame.remove(panel2);
					}
					panel1 = new JPanel();
					panel2 = new JPanel();
					currentPanel = "Buy/Sell";
					createItemsPanel(panel1, "gamePage", 0, 23, 300, 650);
					createItemsPanel(panel2, "itemListing", 305, 23, 300, 650);
					frame.repaint();
					frame.validate();
				} else {
					frame.remove(panel1);
					frame.remove(panel2);
					panel1 = null;
					panel2 = null;
					currentPanel = null;
					frame.repaint();
					frame.validate();
				}
			}
		}); 
		// highlight affect
		btnBuysell.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	btnBuysell.setBackground(new Color(182, 133, 57));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	btnBuysell.setBackground(new Color(142, 93, 27));
		    }
		});
		
		final JButton btnAddremove = new JButton("Add/Remove");
		btnAddremove.setBackground(new Color(66, 39, 9));
		btnAddremove.setForeground(new Color(255, 255, 255));
		btnAddremove.setFont(new Font("Baskerville Old Face", Font.PLAIN, 13));
		btnAddremove.setBounds(85, 0, 103, 23);
		frame.getContentPane().add(btnAddremove);
		btnAddremove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (currentPanel != "Add/Remove") {
					if (panel1 != null) {
						frame.remove(panel1);
					}
					if (panel2 != null) {
						frame.remove(panel2);
					}
					panel1 = new JPanel();
					currentPanel = "Add/Remove";
					createAddRemovePanel(panel1);
					frame.repaint();
					frame.validate();
				} else {
					frame.remove(panel1);
					currentPanel = null;
					frame.repaint();
					frame.validate();
				}
			}
		});
		// highlight affect
		btnAddremove.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	btnAddremove.setBackground(new Color(142, 93, 27));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	btnAddremove.setBackground(new Color(66, 39, 9));
		    }
		});
		
		final JButton btnStatistics = new JButton("Statistics");
		btnStatistics.setBackground(new Color(66, 39, 9));
		btnStatistics.setForeground(new Color(255, 255, 255));
		btnStatistics.setFont(new Font("Baskerville Old Face", Font.PLAIN, 13));
		btnStatistics.setBounds(188, 0, 77, 23);
		frame.getContentPane().add(btnStatistics);
		btnStatistics.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (currentPanel != "Statistics") {
					if (panel1 != null) {
						frame.remove(panel1);
					}
					if (panel2 != null) {
						frame.remove(panel2);
					}
					panel1 = new JPanel();
					currentPanel = "Statistics";
					createStatsPanel(panel1);
					frame.repaint();
					frame.validate();
				} else {
					frame.remove(panel1);
					currentPanel = null;
					frame.repaint();
					frame.validate();
				}
			}
		});
		// highlight affect
		btnStatistics.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	btnStatistics.setBackground(new Color(142, 93, 27));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	btnStatistics.setBackground(new Color(66, 39, 9));
		    }
		});
		
		final JButton btnCustomerSupport = new JButton("Customer Support");
		btnCustomerSupport.setBackground(new Color(66, 39, 9));
		btnCustomerSupport.setForeground(new Color(255, 255, 255));
		btnCustomerSupport.setFont(new Font("Baskerville Old Face", Font.PLAIN, 13));
		btnCustomerSupport.setBounds(265, 0, 132, 23);
		frame.getContentPane().add(btnCustomerSupport);
		btnCustomerSupport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (currentPanel != "Customer Support") {
					if (panel1 != null) {
						frame.remove(panel1);
					}
					if (panel2 != null) {
						frame.remove(panel2);
					}
					panel1 = new JPanel();
					currentPanel = "Customer Support";
					createSupportPanel(panel1);
					frame.repaint();
					frame.validate();
				} else {
					frame.remove(panel1);
					currentPanel = null;
					frame.repaint();
					frame.validate();
				}
			}
		});
		// highlight affect
		btnCustomerSupport.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	btnCustomerSupport.setBackground(new Color(142, 93, 27));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	btnCustomerSupport.setBackground(new Color(66, 39, 9));
		    }
		});
		
		final JButton btnProfile = new JButton("Profile");
		btnProfile.setBackground(new Color(66, 39, 9));
		btnProfile.setForeground(new Color(255, 255, 255));
		btnProfile.setFont(new Font("Baskerville Old Face", Font.PLAIN, 13));
		btnProfile.setBounds(397, 0, 70, 23);
		frame.getContentPane().add(btnProfile);
		btnProfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (currentPanel != "Profile") {
					if (panel1 != null) {
						frame.remove(panel1);
					}
					if (panel2 != null) {
						frame.remove(panel2);
					}
					panel1 = new JPanel();
					currentPanel = "Profile";
					createProfilePanel(panel1);
					frame.repaint();
					frame.validate();
				} else {
					frame.remove(panel1);
					currentPanel = null;
					frame.repaint();
					frame.validate();
				}
			}
		});
		// highlight affect
		btnProfile.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	btnProfile.setBackground(new Color(142, 93, 27));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	btnProfile.setBackground(new Color(66, 39, 9));
		    }
		});
		
		final JButton btnSettings = new JButton("Settings");
		btnSettings.setBackground(new Color(66, 39, 9));
		btnSettings.setForeground(new Color(255, 255, 255));
		btnSettings.setFont(new Font("Baskerville Old Face", Font.PLAIN, 13));
		btnSettings.setBounds(467, 0, 73, 23);
		frame.getContentPane().add(btnSettings);
		btnSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (currentPanel != "Settings") {
					if (panel1 != null) {
						frame.remove(panel1);
					}
					if (panel2 != null) {
						frame.remove(panel2);
					}
					panel1 = new JPanel();
					currentPanel = "Settings";
					createSettingsPanel(panel1);
					frame.repaint();
					frame.validate();
				} else {
					frame.remove(panel1);
					currentPanel = null;
					frame.repaint();
					frame.validate();
				}
			}
		});
		// highlight affect
		btnSettings.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	btnSettings.setBackground(new Color(142, 93, 27));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	btnSettings.setBackground(new Color(66, 39, 9));
		    }
		});
		
		final JButton btnLogOut = new JButton("Log Out");
		btnLogOut.setBackground(new Color(66, 39, 9));
		btnLogOut.setForeground(new Color(255, 255, 255));
		btnLogOut.setFont(new Font("Baskerville Old Face", Font.PLAIN, 13));
		btnLogOut.setBounds(540, 0, 78, 23);
		btnLogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				LoginPage loginPage = new LoginPage();
				loginPage.getFrame().setVisible(true);
			}
		});
		// highlight affect
		btnLogOut.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	btnLogOut.setBackground(new Color(142, 93, 27));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	btnLogOut.setBackground(new Color(66, 39, 9));
		    }
		});
		frame.getContentPane().add(btnLogOut);
	}

	private void createItemsPanel(JPanel panel, String type, int x, int y, int w, int h) {
		panel.setBackground(new Color(61, 61, 61));
		panel.setBounds(x, y, w, h);
		if("gamePage".equals(type)) {
			panel.add(new gamePage(frame));
		}else if("itemListing".equals(type)) {
			panel.add(new itemListing(frame));
		}
		panel.validate();
		panel.repaint();
		frame.getContentPane().add(panel);
		frame.repaint();
		frame.validate();
	}
	
	private void createAddRemovePanel(JPanel panel) {
		panel.setBackground(new Color(13, 20, 36));
		panel.setBounds(54, 18, 300, 650);
		panel.add(new AddItemPage(frame));
		//panel.setLayout(null);
		frame.getContentPane().add(panel);
		frame.repaint();
		frame.validate();
		
		//AddRemoveItem ari = new AddRemoveItem(panel);
	}
	
	private void createStatsPanel(JPanel panel) {
		panel.setBackground(new Color(13, 20, 36));
		panel.setBounds(157, 18, 300, 650);
		//panel.add(new AddItemPage(frame));
		frame.getContentPane().add(panel);
		frame.repaint();
		frame.validate();
	}
	
	private void createSupportPanel(JPanel panel) {
		panel.setBackground(new Color(13, 20, 36));
		panel.setBounds(233, 18, 300, 650);
		panel.add(new CustomerSupportPage(frame));
		frame.getContentPane().add(panel);
		frame.repaint();
		frame.validate();
	}
	
	private void createProfilePanel(JPanel panel) {
		panel.setBackground(new Color(61, 61, 61));
		panel.setBounds(0, 23, 500, 650);
		frame.getContentPane().add(panel);
		frame.repaint();
		frame.validate();
		
		ProfilePage profile = new ProfilePage(panel, account);
	}
	
	private void createSettingsPanel(JPanel panel) {
		panel.setBackground(new Color(13, 20, 36));
		panel.setBounds(434, 18, 300, 650);
		panel.add(new SettingsPage(frame));
		frame.getContentPane().add(panel);
		frame.repaint();
		frame.validate();
	}
	
	public JFrame getFrame() {
		return frame;
	}
	
}
