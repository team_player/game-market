package designerTool;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Component;
import javax.swing.Box;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.FlowLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import net.miginfocom.swing.MigLayout;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollBar;

public class GameMarket {

	private JFrame frame;
	private final Action action = new SwingAction();
	private JTable table;
	private JTable table_1;
	/**
	 * @wbp.nonvisual location=-28,-31
	 */
	private final JScrollBar scrollBar = new JScrollBar();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GameMarket window = new GameMarket();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GameMarket() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setForeground(new Color(0, 0, 0));
		frame.getContentPane().setBackground(new Color(13, 20, 36));
		frame.getContentPane().setLayout(null);
		
		JButton btnBuysell = new JButton("Buy/Sell");
		btnBuysell.setBackground(new Color(142, 93, 27));
		btnBuysell.setForeground(new Color(255, 255, 255));
		btnBuysell.setFont(new Font("Baskerville Old Face", Font.PLAIN, 11));
		btnBuysell.setBounds(0, 0, 72, 23);
		frame.getContentPane().add(btnBuysell);
		
		JButton btnAddremove = new JButton("Add/Remove");
		btnAddremove.setBackground(new Color(66, 39, 9));
		btnAddremove.setForeground(new Color(255, 255, 255));
		btnAddremove.setFont(new Font("Baskerville Old Face", Font.PLAIN, 11));
		btnAddremove.setBounds(72, 0, 94, 23);
		frame.getContentPane().add(btnAddremove);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.setBackground(new Color(66, 39, 9));
		btnSearch.setForeground(new Color(255, 255, 255));
		btnSearch.setFont(new Font("Baskerville Old Face", Font.PLAIN, 11));
		btnSearch.setBounds(166, 0, 65, 23);
		frame.getContentPane().add(btnSearch);
		
		JButton btnStatistics = new JButton("Statistics");
		btnStatistics.setBackground(new Color(66, 39, 9));
		btnStatistics.setForeground(new Color(255, 255, 255));
		btnStatistics.setFont(new Font("Baskerville Old Face", Font.PLAIN, 11));
		btnStatistics.setBounds(231, 0, 73, 23);
		frame.getContentPane().add(btnStatistics);
		
		JButton btnCustomerSupport = new JButton("Customer Support");
		btnCustomerSupport.setBackground(new Color(66, 39, 9));
		btnCustomerSupport.setForeground(new Color(255, 255, 255));
		btnCustomerSupport.setFont(new Font("Baskerville Old Face", Font.PLAIN, 11));
		btnCustomerSupport.setBounds(304, 0, 118, 23);
		frame.getContentPane().add(btnCustomerSupport);
		
		JButton btnProfile = new JButton("Profile");
		btnProfile.setBackground(new Color(66, 39, 9));
		btnProfile.setForeground(new Color(255, 255, 255));
		btnProfile.setFont(new Font("Baskerville Old Face", Font.PLAIN, 11));
		btnProfile.setBounds(419, 0, 64, 23);
		frame.getContentPane().add(btnProfile);
		
		JButton btnSettings = new JButton("Settings");
		btnSettings.setBackground(new Color(66, 39, 9));
		btnSettings.setForeground(new Color(255, 255, 255));
		btnSettings.setFont(new Font("Baskerville Old Face", Font.PLAIN, 11));
		btnSettings.setBounds(483, 0, 69, 23);
		frame.getContentPane().add(btnSettings);
		
		JScrollPane panel = new JScrollPane();
		panel.setBackground(new Color(61, 61, 61));
		panel.setBounds(0, 21, 221, 390);
		frame.getContentPane().add(panel);
		
		frame.setBounds(100, 100, 550, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "Create Account");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		
		}
	}
}
