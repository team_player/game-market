package page;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class AddRemoveItem {
	
	private JPanel panel;
	
	private JButton btnAdd;

	public AddRemoveItem(JPanel panel) {
		this.panel = panel;
		createButtons();
	}
	
	private void createButtons() {
		// add item button
		btnAdd = new JButton("Add Item");
		btnAdd.setBounds(10, 20, 138, 29);
		btnAdd.setForeground(new Color(0, 0, 0));
		btnAdd.setFont(new Font("Britannic Bold", Font.PLAIN, 18));
		panel.add(btnAdd);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		
		// remove item button
		btnAdd = new JButton("Remove Item");
		btnAdd.setBounds(152, 20, 138, 29);
		btnAdd.setForeground(new Color(0, 0, 0));
		btnAdd.setFont(new Font("Britannic Bold", Font.PLAIN, 18));
		panel.add(btnAdd);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
	}
	
}
