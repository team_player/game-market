package page;
import java.util.Vector;
import java.util.Random;
import javax.swing.table.TableColumn;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
public class gamePage extends JPanel {
	private String[][] games =  {{"1"}, {"2"}, {"3"}, {"4"}, {"5"}, 
			{"6"}, {"7"}, {"8"}, {"9"}, {"10"}, 
			{"11"}, {"12"}, {"13"}, {"14"}, {"15"}, 
			{"16"}, {"17"}, {"18"}, {"19"}, {"20"}, 
			{"21"}, {"22"}, {"23"}};
    String[] columnNames = {"Games"};
	private JFrame frame;
	
	public gamePage(JFrame frame) {
		this.frame = frame;
        initializeUI();
    }

    private void initializeUI() {
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(300, 1000));
        DefaultTableModel model = new DefaultTableModel(games, columnNames){

            @Override
            public boolean isCellEditable(int row, int column) {
               return false;
            }
        }; // change 40 to numGames
        final JTable table = new JTable(model);
        table.setRowHeight(25);
        JScrollPane pane = new JScrollPane(table);
        
        // customize
        table.setBackground(new Color(61, 61, 61));
        table.setGridColor(new Color(81, 81, 81));    
        table.setForeground(new Color(255, 255, 255));
        table.setSelectionBackground(new Color(182, 143, 67));
        table.setSelectionForeground(new Color(255, 255, 255));
        table.getTableHeader().setBackground(new Color(142, 93, 27));
        table.getTableHeader().setForeground(new Color(255, 255, 255));
        table.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 12));
        table.setFont(new Font("Tahoma", Font.PLAIN, 12));
        
        table.addMouseListener(new MouseAdapter() {
        	 public void mouseClicked(MouseEvent e) {
        	    if (e.getClickCount() == 2) {
        	      JTable target = (JTable)e.getSource();
        	      int row = target.getSelectedRow();
        	      int column = target.getSelectedColumn();
        	      // populate item table
        	    }
        	 }
        });
        
        // add it
        add(pane, BorderLayout.CENTER);
    }   
}