package page;
import java.util.Vector;

public class Game {
	private String name;
	private static int numGames = 0;
	private int numItems;
	private Vector<Item> items;
	public Game(String name) {
		super();
		this.name = name;
		numGames++; 
		numItems=0;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public static int getNumGames() {
		return numGames;
	}
	public static void setNumGames(int numGames) {
		Game.numGames = numGames;
	}
	public Vector<Item> getItems() {
		return items;
	}
	public void setItems(Vector<Item> items) {
		this.items = items;
	}
}
