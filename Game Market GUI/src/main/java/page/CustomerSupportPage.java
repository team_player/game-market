package page;
import java.util.Vector;
import java.util.Random;
import javax.swing.table.TableColumn;

import actors.User;
import mainPage.MainPage;

import javax.swing.table.DefaultTableModel;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
public class CustomerSupportPage extends JPanel {
	private JFrame frame;
	private String[] categories = {"Bug", "How to", "Transactions", "Technical Issue", "Cancellation", "Other"};
	
	public CustomerSupportPage(JFrame frame) {
		this.frame = frame;
        initializeUI();
    }

    private void initializeUI() {
    	JComboBox categoryList = new JComboBox(categories);
    	categoryList.setSelectedIndex(4);
    	categoryList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		        JComboBox cb = (JComboBox)e.getSource();
		        String cat = (String)cb.getSelectedItem();
		        if(cat.equals("Bug")) {
		        	
		        }else if(cat.equals("How to")) {
		        	
		        }else if(cat.equals("Transactions")) {
		        	
		        }else if(cat.equals("Technical Issue")) {
		        	
		        }else if(cat.equals("Cancellation")) {
		        	
		        }else if(cat.equals("Other")) {
		        	
		        }
			}
		});
        // category label
		JLabel catLabel = new JLabel(" Category: ");
		catLabel.setBounds(50, 90, 200, 29);
		catLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		catLabel.setForeground(new Color(255, 255, 255));
		catLabel.setBackground(new Color(61, 61, 61));
		catLabel.setOpaque(true);
        add(catLabel, BorderLayout.CENTER);
    	// drop down menu
    	categoryList.setBackground(new Color(61, 61, 61)); 
    	categoryList.setForeground(new Color(255, 255, 255));
        add(categoryList, BorderLayout.CENTER);
        
        // grid
    	setLayout(new GridLayout(3, 2));
        
        // message label
 		JLabel emptyBlock = new JLabel(" Message: ");
 		emptyBlock.setBounds(50, 90, 200, 29);
 		emptyBlock.setFont(new Font("Tahoma", Font.BOLD, 12));
 		emptyBlock.setForeground(new Color(255, 255, 255));
 		emptyBlock.setBackground(new Color(61, 61, 61));
 		emptyBlock.setOpaque(true);
        add(emptyBlock, BorderLayout.CENTER);
		// message field
		JTextField messageField = new JTextField();
		messageField.setFont(new Font("Tahoma", Font.BOLD, 12));
		messageField.setBounds(0, 0, 270, 29);
        add(messageField, BorderLayout.WEST);
        //buttons
        // submit
		final JButton btnSubmit = new JButton("submit");
		btnSubmit.setBounds(230, 335, 90, 29);
		btnSubmit.setForeground(new Color(255, 255, 255));
		btnSubmit.setBackground(new Color(0, 0, 126));
		btnSubmit.setFont(new Font("Tahoma", Font.BOLD, 12));
		//frame.getContentPane().add(btnCreateAccount);
		add(btnSubmit, BorderLayout.CENTER);
		final CustomerSupportPage t = this;
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				t.setVisible(false);
			}
		});
		btnSubmit.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	btnSubmit.setBackground(new Color(0, 0, 216));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	btnSubmit.setBackground(new Color(0, 0, 126));
		    }
		});
		// cancel
		final JButton btnCancel = new JButton("Cancel");
		btnCancel.setBounds(230, 335, 90, 29);
		btnCancel.setForeground(new Color(255, 255, 255));
		btnCancel.setBackground(new Color(126, 0, 0));
		btnCancel.setFont(new Font("Tahoma", Font.BOLD, 12));
		//frame.getContentPane().add(btnCreateAccount);
		add(btnCancel, BorderLayout.CENTER);
		final CustomerSupportPage t2 = this;
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				t2.setVisible(false);
			}
		});
		btnCancel.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	btnCancel.setBackground(new Color(216, 0, 0));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	btnCancel.setBackground(new Color(126, 0, 0));
		    }
		});
    }   
}
