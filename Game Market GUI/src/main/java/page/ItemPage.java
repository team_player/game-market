package page;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
public class ItemPage {
	private JPanel buttons = null;
	
	public JPanel guiGenerate(final JPanel frame) {
		
		frame.setLayout(null);
		
		final JPanel backGround = new JPanel();
//		backGround.setBackground(new Color(61, 61, 61));
//		backGround.setBounds(275, 236, 200, 200);
		backGround.setBackground(new Color(0, 0, 0, 200));
		backGround.setBounds(0, 23, 750, 650);
		frame.add(backGround);
		
		JPanel info = new JPanel();
		JPanel labels = new JPanel();
		JPanel fields = new JPanel();
		
		info.setLayout(new BoxLayout(info, BoxLayout.X_AXIS));
		labels.setLayout(new BoxLayout(labels, BoxLayout.Y_AXIS));
		fields.setLayout(new BoxLayout(fields, BoxLayout.Y_AXIS));
		
		
		JLabel lName = new JLabel("Name: ", SwingConstants.LEFT);
		JLabel lID = new JLabel("ID: ", SwingConstants.LEFT);
		JLabel lDescription = new JLabel("Description: ", SwingConstants.LEFT);
		labels.add(lName);
		labels.add(lID);
		labels.add(lDescription);
		
		
		JTextField name = new JTextField();
		name.setEditable(false);
		JTextField id = new JTextField();
		id.setEditable(false);
		JTextField description = new JTextField();
		description.setEditable(false);
		fields.add(name);
		fields.add(id);
		fields.add(description);
		
		info.add(labels);
		info.add(fields);
		
		buttons = new JPanel();
		final JButton bid = new JButton("Bid");
		bid.setBackground(new Color(142, 93, 27));
		bid.setForeground(new Color(255, 255, 255));
		bid.setFont(new Font("Baskerville Old Face", Font.PLAIN, 13));
		bid.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		bid.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	bid.setBackground(new Color(182, 133, 57));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	bid.setBackground(new Color(142, 93, 27));
		    }
		});
		final JButton buyout = new JButton("Buyout");
		buyout.setBackground(new Color(0, 0, 126));
		buyout.setForeground(new Color(255, 255, 255));
		buyout.setFont(new Font("Baskerville Old Face", Font.PLAIN, 13));
		buyout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		buyout.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	buyout.setBackground(new Color(0, 0, 216));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	buyout.setBackground(new Color(0, 0, 126));
		    }
		});
		final JButton cancel = new JButton("cancel");
		cancel.setBackground(new Color(126, 0, 0));
		cancel.setForeground(new Color(255, 255, 255));
		cancel.setFont(new Font("Tahoma", Font.BOLD, 12));
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				backGround.setVisible(false);
			}
		});
		cancel.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	cancel.setBackground(new Color(216, 0, 0));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	cancel.setBackground(new Color(126, 0, 0));
		    }
		});
		
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.X_AXIS));
		buttons.add(bid);
		buttons.add(buyout);
		buttons.add(cancel);
		backGround.repaint();
		
		//Space left for possible image
		
		backGround.removeAll();
		backGround.setLayout(new GridBagLayout());
		GridBagConstraints constraint = new GridBagConstraints();
		constraint.gridx = 1;
		constraint.gridy = 0;
		constraint.gridwidth = 2;
		backGround.add(info, constraint);
		constraint.gridx = 0;
		constraint.gridy = 1;
		constraint.gridwidth = GridBagConstraints.REMAINDER;
		backGround.add(buttons, constraint);
		backGround.revalidate();
		backGround.repaint();
		//Return frame to remodify it
		return backGround;
	}
}
