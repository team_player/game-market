package page;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import actors.User;
import login.LoginPage;
import mainPage.MainPage;

public class ProfilePage {
	
	JPanel panel;
	
	private JTextField firstNameField;
	private JTextField lastNameField;
	private JTextField usernameField;
	private JTextField emailField;
	
	private JLabel firstName;
	private JLabel lastName;
	private JLabel username;
	private JLabel email;
	private JTextArea bio;
	
	private JButton btn;
	
	private final Color WHITE = new Color(255, 255, 255);

	private User account;

	public ProfilePage(JPanel panel, User account) {
		this.panel = panel;
		this.account = account;
		
		panel.setLayout(null);
		createLabels();
		printInfo();
		initialButtons();
	}
	
	private void createFields() {
		// first name text field
		firstNameField = new JTextField(account.getAccount().getFirstName());
		firstNameField.setFont(new Font("Arial", Font.PLAIN, 18));
		firstNameField.setBounds(208, 90, 270, 29);
		panel.add(firstNameField);
		
		// last name text field
		lastNameField = new JTextField(account.getAccount().getLastName());
		lastNameField.setFont(new Font("Arial", Font.PLAIN, 18));
		lastNameField.setBounds(208, 129, 270, 29);
		panel.add(lastNameField);
		
		// username text field
		usernameField = new JTextField(account.getUsername());
		usernameField.setFont(new Font("Arial", Font.PLAIN, 18));
		usernameField.setBounds(208, 168, 270, 29);
		panel.add(usernameField);
		
		// email address text field
		emailField = new JTextField(account.getAccount().getEmail());
		emailField.setFont(new Font("Arial", Font.PLAIN, 18));
		emailField.setBounds(208, 207, 270, 29);
		panel.add(emailField);
		
		// biography text field
		bio = new JTextArea(account.getAccount().getBio());
		bio.setBounds(70, 278, 400, 100);
		bio.setFont(new Font("Arial", Font.PLAIN, 17));
		bio.setForeground(new Color(0, 0, 0));
		bio.setLineWrap(true);
		bio.setEditable(true);
		bio.setBackground(WHITE);
		panel.add(bio);
	}
	
	private void removeFields() {
		panel.remove(firstNameField);
		panel.remove(lastNameField);
		panel.remove(usernameField);
		panel.remove(emailField);
		panel.remove(bio);
	}
	
	private void createLabels() {
		// Create Account title
		JLabel createAccount = new JLabel("User Profile");
		createAccount.setBounds(179, 35, 142, 20);
		createAccount.setFont(new Font("Britannic Bold", Font.PLAIN, 27));
		createAccount.setForeground(WHITE);
		panel.add(createAccount);
		
		// First name label
		JLabel firstNameLabel = new JLabel("First Name: ");
		firstNameLabel.setBounds(50, 90, 200, 29);
		firstNameLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		firstNameLabel.setForeground(WHITE);
		panel.add(firstNameLabel);
		
		// Last name label
		JLabel lastNameLabel = new JLabel("Last Name:");
		lastNameLabel.setBounds(50, 129, 200, 29);
		lastNameLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		lastNameLabel.setForeground(WHITE);
		panel.add(lastNameLabel);
		
		// username label
		JLabel usernameLabel = new JLabel("username:");
		usernameLabel.setBounds(50, 168, 200, 29);
		usernameLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		usernameLabel.setForeground(WHITE);
		panel.add(usernameLabel);
		
		// email address label
		JLabel emailLabel = new JLabel("email address:");
		emailLabel.setBounds(50, 207, 200, 29);
		emailLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		emailLabel.setForeground(WHITE);
		panel.add(emailLabel);
		
		// biography label
		JLabel bioLabel = new JLabel("biography:");
		bioLabel.setBounds(50, 246, 200, 29);
		bioLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		bioLabel.setForeground(WHITE);
		panel.add(bioLabel);
	}
	
	private void printInfo() {
		firstName = new JLabel(account.getAccount().getFirstName());
		firstName.setBounds(220, 90, 200, 29);
		firstName.setFont(new Font("Arial", Font.PLAIN, 20));
		firstName.setForeground(WHITE);
		panel.add(firstName);

		lastName = new JLabel(account.getAccount().getLastName());
		lastName.setBounds(220, 129, 200, 29);
		lastName.setFont(new Font("Arial", Font.PLAIN, 20));
		lastName.setForeground(WHITE);
		panel.add(lastName);

		username = new JLabel(account.getUsername());
		username.setBounds(220, 168, 200, 29);
		username.setFont(new Font("Arial", Font.PLAIN, 20));
		username.setForeground(WHITE);
		panel.add(username);

		email = new JLabel(account.getAccount().getEmail());
		email.setBounds(220, 207, 270, 29);
		email.setFont(new Font("Arial", Font.PLAIN, 20));
		email.setForeground(WHITE);
		panel.add(email);

		bio = new JTextArea(account.getAccount().getBio());
		bio.setBounds(70, 278, 400, 100);
		bio.setFont(new Font("Arial", Font.PLAIN, 17));
		bio.setForeground(WHITE);
		bio.setLineWrap(true);
		bio.setEditable(false);
		bio.setBackground(new Color(61, 61, 61));
		panel.add(bio);
	}
	
	private void removeText() {
		panel.remove(firstName);
		panel.remove(lastName);
		panel.remove(username);
		panel.remove(email);
		panel.remove(bio);
	}
	
	private void initialButtons() {
		// edit account button
		btn = new JButton("Edit Account");
		btn.setBounds(183, 530, 134, 29);
		btn.setForeground(new Color(0, 0, 0));
		btn.setFont(new Font("Britannic Bold", Font.PLAIN, 18));
		panel.add(btn);
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panel.remove(btn);
				editButtons();
				removeText();
				createFields();
				
				panel.repaint();
				panel.validate();
			}
		});
	}
	
	private void editButtons() {
		// save changes button
		btn = new JButton("Save Changes");
		btn.setBounds(178, 530, 145, 29);
		btn.setForeground(new Color(0, 0, 0));
		btn.setFont(new Font("Britannic Bold", Font.PLAIN, 18));
		panel.add(btn);
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveChanges();
				
				panel.remove(btn);
				initialButtons();
				removeFields();
				printInfo();
				
				panel.repaint();
				panel.validate();
			}
		});
	}
	
	private void saveChanges() {
		account.getAccount().setFirstName(firstNameField.getText());
		account.getAccount().setLastName(lastNameField.getText());
		account.setUsername(usernameField.getText());
		account.getAccount().setEmail(emailField.getText());
		account.getAccount().setBio(bio.getText());
	}
	
}
