package page;
import java.util.Vector;
import java.util.Random;
import javax.swing.table.TableColumn;

import actors.User;
import mainPage.MainPage;

import javax.swing.table.DefaultTableModel;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
public class AddItemPage extends JPanel {
	private JFrame frame;
	private String[] itemListing = {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "..."};
	private String[] operation = {"Add", "Remove"};
	
	public AddItemPage(JFrame frame) {
		this.frame = frame;
        initializeUI();
    }

    private void initializeUI() {
        // grid
    	setLayout(new GridLayout(6, 2));
    	
    	// item drop menu operations
    	JComboBox items = new JComboBox(itemListing);
    	items.setSelectedIndex(4);
    	items.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		        JComboBox cb = (JComboBox)e.getSource();
		        String cat = (String)cb.getSelectedItem();
		        if(cat.equals("some string")) {
			        // do something
		        }
			}
		});
        // category label
		JLabel catLabel = new JLabel(" Item: ");
		catLabel.setBounds(50, 90, 200, 29);
		catLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		catLabel.setForeground(new Color(255, 255, 255));
		catLabel.setBackground(new Color(61, 61, 61));
		catLabel.setOpaque(true);
        add(catLabel, BorderLayout.CENTER);
    	// drop down menu
        items.setBackground(new Color(61, 61, 61)); 
        items.setForeground(new Color(255, 255, 255));
        add(items, BorderLayout.CENTER);
        
        // labels & fields
        // buyout label
 		JLabel buyoutLabel = new JLabel(" Buyout Price: ");
 		buyoutLabel.setBounds(50, 90, 200, 29);
 		buyoutLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
 		buyoutLabel.setForeground(new Color(255, 255, 255));
 		buyoutLabel.setBackground(new Color(61, 61, 61));
 		buyoutLabel.setOpaque(true);
        add(buyoutLabel, BorderLayout.CENTER);
		// buyout field
		JTextField messageField = new JTextField();
		messageField.setFont(new Font("Tahoma", Font.BOLD, 12));
		messageField.setBounds(0, 0, 270, 29);
        add(messageField, BorderLayout.WEST);
        // bid label
  		JLabel bidLabel = new JLabel(" Bid Starting Price: ");
  		bidLabel.setBounds(50, 90, 200, 29);
  		bidLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
  		bidLabel.setForeground(new Color(255, 255, 255));
  		bidLabel.setBackground(new Color(61, 61, 61));
  		bidLabel.setOpaque(true);
        add(bidLabel, BorderLayout.CENTER);
 		// bid field
 		JTextField bidField = new JTextField();
 		bidField.setFont(new Font("Tahoma", Font.BOLD, 12));
 		bidField.setBounds(0, 0, 270, 29);
         add(bidField, BorderLayout.WEST);
        // start date label
   		JLabel startDateLabel = new JLabel(" Starting Date: ");
   		startDateLabel.setBounds(50, 90, 200, 29);
   		startDateLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
   		startDateLabel.setForeground(new Color(255, 255, 255));
   		startDateLabel.setBackground(new Color(61, 61, 61));
   		startDateLabel.setOpaque(true);
        add(startDateLabel, BorderLayout.CENTER);
  		// start date field
  		JTextField startDateField = new JTextField();
  		startDateField.setFont(new Font("Tahoma", Font.BOLD, 12));
  		startDateField.setBounds(0, 0, 270, 29);
        add(startDateField, BorderLayout.WEST);
        // end date label
   		JLabel endDateLabel = new JLabel(" Ending date: ");
   		endDateLabel.setBounds(50, 90, 200, 29);
   		endDateLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
   		endDateLabel.setForeground(new Color(255, 255, 255));
   		endDateLabel.setBackground(new Color(61, 61, 61));
   		endDateLabel.setOpaque(true);
        add(endDateLabel, BorderLayout.CENTER);
  		// end date field
  		JTextField endDateField = new JTextField();
  		endDateField.setFont(new Font("Tahoma", Font.BOLD, 12));
  		endDateField.setBounds(0, 0, 270, 29);
        add(endDateField, BorderLayout.WEST);
         
        //buttons
        // submit
		final JButton btnSubmit = new JButton("submit");
		btnSubmit.setBounds(230, 335, 90, 29);
		btnSubmit.setForeground(new Color(255, 255, 255));
		btnSubmit.setBackground(new Color(0, 0, 126));
		btnSubmit.setFont(new Font("Tahoma", Font.BOLD, 12));
		//frame.getContentPane().add(btnCreateAccount);
		add(btnSubmit, BorderLayout.CENTER);
		final AddItemPage t = this;
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				t.setVisible(false);
			}
		});
		btnSubmit.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	btnSubmit.setBackground(new Color(0, 0, 216));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	btnSubmit.setBackground(new Color(0, 0, 126));
		    }
		});
		// cancel
		final JButton btnCancel = new JButton("Cancel");
		btnCancel.setBounds(230, 335, 90, 29);
		btnCancel.setForeground(new Color(255, 255, 255));
		btnCancel.setBackground(new Color(126, 0, 0));
		btnCancel.setFont(new Font("Tahoma", Font.BOLD, 12));
		//frame.getContentPane().add(btnCreateAccount);
		add(btnCancel, BorderLayout.CENTER);
		final AddItemPage t2 = this;
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { 
				t2.setVisible(false);
			}
		});
		btnCancel.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	btnCancel.setBackground(new Color(216, 0, 0));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	btnCancel.setBackground(new Color(126, 0, 0));
		    }
		});
    }   
}
