package page;
import java.util.Vector;
import java.util.Random;
import javax.swing.table.TableColumn;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
public class itemListing extends JPanel {
	private String[][] items = {{"1"}, {"2"}, {"3"}, {"4"}, {"5"}, 
			{"6"}, {"7"}, {"8"}, {"9"}, {"10"}, 
			{"11"}, {"12"}, {"13"}, {"14"}, {"15"}, 
			{"16"}, {"17"}, {"18"}, {"19"}, {"20"}, 
			{"21"}, {"22"}, {"23"}};
    String[] columnNames = {"Items: "+items.length};
	private JFrame frame;
	
	public itemListing(JFrame frame) {
		this.frame = frame;
        initializeUI();
    }

    private void initializeUI() {
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(300, 1000));
        DefaultTableModel model = new DefaultTableModel(items, columnNames){

            @Override
            public boolean isCellEditable(int row, int column) {
               return false;
            }
        }; // change 40 to numGames
        JTable table = new JTable(model){
        	@Override
        	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        	Component comp = super.prepareRenderer(renderer, row, column);
        	comp.setBackground(row % 2 == 0 ? new Color(61, 61, 61) : new Color(101, 101, 101));
        	return comp;
        	}
        };
        table.setRowHeight(25);
        JScrollPane pane = new JScrollPane(table);
        //table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        // customize
        //table.setBackground(new Color(61, 61, 61));
        table.setGridColor(new Color(81, 81, 81));    
        table.setForeground(new Color(255, 255, 255));
        table.setSelectionBackground(new Color(182, 143, 67));
        table.setSelectionForeground(new Color(255, 255, 255));
        table.getTableHeader().setBackground(new Color(142, 93, 27));
        table.getTableHeader().setForeground(new Color(255, 255, 255));
        table.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 12));
        table.setFont(new Font("Tahoma", Font.PLAIN, 12));
        
        table.addMouseListener(new MouseAdapter() {
        	public void mouseClicked(MouseEvent e) {
        		if (e.getClickCount() == 2) {
        	    	JTable target = (JTable)e.getSource();
        	      	int row = target.getSelectedRow();
        	      	int column = target.getSelectedColumn();
        	      	ItemPage ip = new ItemPage();
        	      	
        	      	JPanel t = new JPanel();
        			t.setBackground(new Color(0, 0, 0, 200));
        			t.setBounds(0, 23, 750, 650);
        			t = ip.guiGenerate(t); 
        			frame.getContentPane().add(t);
        			frame.validate();
        			frame.repaint();
        		}
        	}
        });
        add(pane, BorderLayout.CENTER);
    }
}