package startUp;

import login.*;
import mainPage.*;

import java.awt.Color;

import javax.swing.JFrame;

import actors.User;

public class StartUp {
	
	public static void main(String[] args) {
		StartUp window = new StartUp();
		
//		User user = new User("Elissa","Skinner","elissa_skinner1@baylor.edu","Imdabest","this is my username");
//		MainPage mainPage = new MainPage(user);
//		mainPage.getFrame().setVisible(true);
		
		LoginPage loginPage = new LoginPage();
		loginPage.getFrame().setVisible(true);
	}
	
}
