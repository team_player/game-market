-- --------------------------------------------------------------------------
-- Author: Jackson O'Donnell
-- 
-- Description: SQL script to make the database for the suede road
-- --------------------------------------------------------------------------

-- make or create the database
DROP DATABASE IF EXISTS `suederoad`;
CREATE DATABASE IF NOT EXISTS `suederoad`;

USE `suederoad`;

-- -----------------------------------------------------------------------
-- Create the relations within the database
-- -----------------------------------------------------------------------

-- -----------------------------------------------------------------------
-- Table: userinfo
-- 
-- Description: Holds information about the user's account
-- 
-- Attributes: fName - the user's first name
-- 	       lName - the user's last name
--	       uName - the user's username (must be unique among users)
-- 	       passwd - the user's password
-- 	       bio - the user's description (set by user)
-- -----------------------------------------------------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE IF NOT EXISTS `userinfo` (
	`fName` VARCHAR(25) NOT NULL,
	`lName` VARCHAR(25) NOT NULL,
	`uName` VARCHAR(25) NOT NULL,
	`passwd` VARCHAR(25) NOT NULL,
	`bio` VARCHAR (255) NOT NULL,
	PRIMARY KEY (`uName`)
	);

-- -----------------------------------------------------------------------
-- Table: userinventory
-- 
-- Description: Holds information about the user's inventory/items
--
-- Attributes: uName - the user's username
-- 	       item - the name of the item
-- 	       game - the name of the game that the item is from
-- 	       quantity - the number of the item that the user has
-- -----------------------------------------------------------------------
DROP TABLE IF EXISTS `userinventory`;
CREATE TABLE IF NOT EXISTS `userinventory` (
	`uName` VARCHAR(25) NOT NULL,
	`item` VARCHAR(25) NOT NULL,
	`game` VARCHAR(25) NOT NULL, 
	`quantity` INT NOT NULL,
	PRIMARY KEY (`uName`, `item`, `game`)
	);

-- -----------------------------------------------------------------------
-- Table: allitems
--
-- Description: Holds information about all possible items
-- 
-- Attributes: item - the name of the item
-- 	       game - the name of the game the item is from
-- -----------------------------------------------------------------------
DROP TABLE IF EXISTS `allitems`;
CREATE TABLE IF NOT EXISTS `allitems` (
	`item` VARCHAR(25) NOT NULL,
	`game` VARCHAR(25) NOT NULL,
	PRIMARY KEY (`item`, `game`)
	);

-- -----------------------------------------------------------------------
-- Table: listeditems
-- 
-- Description: Holds information about all the items currently listed
-- 
-- Attributes: item - the name of the item
-- 	       game - the name of hte game the item is from
-- 	       quantity - the number of the item that is being sold by the user
-- 	       uName - the seller's username
-- 	       ppea - the price per item
-- -----------------------------------------------------------------------
DROP TABLE IF EXISTS `listeditems`;
CREATE TABLE IF NOT EXISTS `listeditems` (
	`item` VARCHAR(25) NOT NULL,
	`game` VARCHAR(25) NOT NULL,
	`quantity` INT NOT NULL,
	`uName` VARCHAR(25) NOT NULL,
	`ppea` FLOAT NOT NULL,
	PRIMARY KEY(`uName`, `item`, `game`)
	);


-- -----------------------------------------------------------------------
-- Table: orderitems
-- 
-- Description: Holds information about all the items currently being ordered
-- 
-- Attributes: item - the name of the item
-- 	       game - the name of hte game the item is from
-- 	       quantity - the number of the item that is being sold by the user
-- 	       uName - the buyer's username
-- 	       ppea - the price per item
-- -----------------------------------------------------------------------
DROP TABLE IF EXISTS `orderitems`;
CREATE TABLE IF NOT EXISTS `orderitems` (
	`item` VARCHAR(25) NOT NULL,
	`game` VARCHAR(25) NOT NULL,
	`quantity` INT NOT NULL,
	`uName` VARCHAR(25) NOT NULL,
	`ppea` FLOAT NOT NULL,
	PRIMARY KEY(`uName`, `item`, `game`)
	);



