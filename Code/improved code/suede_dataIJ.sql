-- -----------------------------------------------------------
-- Author: Jackson O'Donnell
--
-- Purpose: Create dummy data for testing
-- -----------------------------------------------------------

connect 'jdbc:derby:.\suedeDB';

INSERT INTO userinfo VALUES
   ('Jack', 'Odonnell', 'Jack', 'password', 'is an awesome dev', 12),
   ('Elissa', 'Skinner', 'xXxSkinnerxXx', 'password', 'is most likely human', 13),
   ('Max', 'Caddedu', 'ThE69CaDdY69mAn', 'password', 'is a poo poo head', 14.3),
   ('Jeff', 'Salad', 'SaladFingers', 'password', 'likes rusty spoons', 15);
   
INSERT INTO allgames VALUES
   ('rust'),
   ('Warframe'),
   ('osrs'),
   ('Skyrim'),
   ('WoW'),
   ('minecraft'),
   ('Rick & Morty II');
   
INSERT INTO allitems VALUES
   ('plumbus', 'Rick & Morty II'),
   ('Forma', 'Warframe'),
   ('rusty spoon', 'rust'),
   ('red water', 'minecraft'),
   ('rune scimitar', 'osrs'),
   ('linen bandages', 'WoW'),
   ('Time-Lost Proto Drake', 'WoW'),
   ('Akjagara', 'Warframe'),
   ('skeleton key', 'Skyrim');

INSERT INTO userinventory VALUES
   ('Jack', 'plumbus', 'Rick & Morty II', 10),
   ('Jack', 'Forma', 'Warframe', 69),
   ('SaladFingers', 'rusty spoon', 'rust', 16),
   ('SaladFingers', 'red water', 'minecraft', 6007),
   ('xXxSkinnerxXx', 'rune scimitar', 'osrs', 1),
   ('xXxSkinnerxXx', 'linen bandages', 'WoW', 800),
   ('xXxSkinnerxXx', 'Time-Lost Proto Drake', 'WoW', 1);

INSERT INTO listeditems VALUES
   ('red water', 'minecraft', 56, 'SaladFingers', 12.96),
   ('Forma', 'Warframe', 1, 'Jack', 1.00),
   ('plumbus', 'Rick & Morty II', 10, 'Jack', 0.01),
   ('rune scimitar', 'osrs', 1, 'xXxSkinnerxXx', 27.89),
   ('Time-Lost Proto Drake', 'WoW', 1, 'xXxSkinnerxXx', 100000.00);

INSERT INTO orderitems VALUES
   ('rusty spoon', 'rust', 99, 'SaladFingers', 100.27),
   ('Forma', 'Warframe', 34, 'xXxSkinnerxXx', 0.50),
   ('Akjagara', 'Warframe', 27, 'Jack', 0.25),
   ('skeleton key', 'Skyrim', 1, 'Jack', 140.00),
   ('rune scimitar', 'osrs', 29, 'ThE69CaDdY69mAn', 0.01);
