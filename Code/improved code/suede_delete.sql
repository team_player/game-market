-- --------------------------------------------
-- Author: Jackson O'Donnell
--
-- Purpose: To remove all data from database
-- --------------------------------------------

connect 'jdbc:derby:.\suedeDB';

DELETE FROM userinfo;
DELETE FROM allgames;
DELETE FROM allitems;
DELETE FROM userinventory;
DELETE FROM listeditems;
DELETE FROM orderitems;
