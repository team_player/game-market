package testUser;

import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.junit.Assert;
import org.junit.Test;

import database.Database;
import user.UserCreationControler;

/*
 * Tests the UserCreationController Class
 * @author Elissa Skinner
 */

public class UserCreationControllerTester {
	
	UserCreationControler ucc = new UserCreationControler();

	@Test
	public void createValidUser() {
		boolean b = Database.executeInstruction("Delete FROM userinfo WHERE uName = 'TestCase'");
		Assert.assertTrue(ucc.createUser(new JTextField("Albert"), 
									new JTextField("Einstien"),
									new JTextField("TestCase"),
									new JPasswordField("password")));
	}
	
	@Test
	public void createUserEmptyField() {
		Database.executeInstruction("Delete FROM userinfo WHERE uName = 'TestCase'");
		Assert.assertFalse(ucc.createUser(new JTextField("Beyonce"), 
									new JTextField(""),
									new JTextField("TestCase"),
									new JPasswordField("password")));
	}
	
	@Test
	public void createUserExceedsCharCount() {
		Database.executeInstruction("Delete FROM userinfo WHERE uName = 'TestCase'");
		Assert.assertFalse(ucc.createUser(new JTextField("Burt"), 
						new JTextField("Supercalifragilisticexpialidocious"),
						new JTextField("TestCase"),
						new JPasswordField("password")));
	}
	
	@Test
	public void editUninitializedUser() {
		Database.executeInstruction("Delete FROM userinfo WHERE uName = 'TestCase'");
		Assert.assertFalse(ucc.editUser(new JTextField("Tomas"), 
									new JTextField("Cerny"),
									new JLabel("TestCase"),
									new JTextField("I love Software!")));
	}
	
	@Test
	public void SuccessfullyEditUser() {
		ucc.createUser(new JTextField("Albert"), new JTextField("Einstien"), 
				new JTextField("TestCase"), new JPasswordField("password"));
		
		Assert.assertTrue(ucc.editUser(new JTextField("Tomas"), 
					new JTextField("Cerny"),
					new JLabel("TestCase"),
					new JTextField("I love Software!")));
	}
	
	@Test
	public void editUserEmptyField() {
		ucc.createUser(new JTextField("Albert"), new JTextField("Einstien"), 
				new JTextField("TestCase"), new JPasswordField("password"));
		
		Assert.assertFalse(ucc.editUser(new JTextField("Beyonce"), 
					new JTextField(""),
					new JLabel("TestCase"),
					new JTextField("I love Software!")));
	}
	
	@Test
	public void editUserExceedsCharCount() {
		ucc.createUser(new JTextField("Albert"), new JTextField("Einstien"), 
				new JTextField("TestCase"), new JPasswordField("password"));
		
		Assert.assertFalse(ucc.editUser(new JTextField("Burt"), 
					new JTextField("Supercalifragilisticexpialidocious"),
					new JLabel("TestCase"),
					new JTextField("I love Software!")));
	}
	
	@Test
	public void deleteUser() {
		ucc.createUser(new JTextField("Albert"), new JTextField("Einstien"), 
				new JTextField("TestCase"), new JPasswordField("password"));
		ucc.deleteUser("TestCase");
		Assert.assertFalse(ucc.editUser(new JTextField("Tomas"), 
					new JTextField("Cerny"),
					new JLabel("TestCase"),
					new JTextField("I love Software!")));
	}
	
	@Test
	public void successfulLogIn() {
		ucc.createUser(new JTextField("Albert"), new JTextField("Einstien"), 
				new JTextField("TestCase"), new JPasswordField("password"));
		Assert.assertTrue(ucc.logIn("TestCase", new JPasswordField("password")));
	}
	
}
