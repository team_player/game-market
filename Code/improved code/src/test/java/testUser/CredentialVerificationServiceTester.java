package testUser;

import java.sql.ResultSet;

import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.junit.Assert;
import org.junit.Test;

import user.CredentialVerificationService;
import user.UserCreationControler;

/*
 * Tests the CredentialVerificationService Class
 * @author Elissa Skinner
 */

public class CredentialVerificationServiceTester {

	@Test
	public void testCorrectPassword() {
		UserCreationControler ucc = new UserCreationControler();
		ucc.deleteUser("TestCase");
		ucc.createUser(new JTextField("Albert"), new JTextField("Einstien"), 
				new JTextField("TestCase"), new JPasswordField("password"));
		ResultSet rs = CredentialVerificationService.verifyPassword("TestCase", new JPasswordField("password"));
		Assert.assertTrue(rs != null);
	}
	
	@Test
	public void testIncorrectPassword() {
		UserCreationControler ucc = new UserCreationControler();
		ucc.deleteUser("TestCase");
		ucc.createUser(new JTextField("Albert"), new JTextField("Einstien"), 
				new JTextField("TestCase"), new JPasswordField("password"));
		ResultSet rs = CredentialVerificationService.verifyPassword("TestCase", new JPasswordField("p@ssword"));
		Assert.assertTrue(rs == null);
	
	}
	
	@Test
	public void testFieldEmpty() {
		Assert.assertFalse(CredentialVerificationService.verifyFilledField(new JTextField()));
	}
	
	@Test
	public void testFieldFilled() {
		Assert.assertTrue(CredentialVerificationService.verifyFilledField(new JTextField("Cerny")));
	}
	
	@Test
	public void testFieldLengthZero() {
		Assert.assertFalse(CredentialVerificationService.verifyFieldLengthRequirements(new JTextField()));
	}
	
	@Test
	public void testFieldLength26() {
		Assert.assertFalse(CredentialVerificationService.verifyFieldLengthRequirements(new JTextField("abcdefghijklmnopqrstuvwxyz")));
	}
	
	@Test
	public void TestFieldAppropriateLength() {
		Assert.assertTrue(CredentialVerificationService.verifyFieldLengthRequirements(new JTextField("Cerny")));
	}
	
}
