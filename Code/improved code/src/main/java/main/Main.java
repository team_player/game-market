package main;

import java.util.logging.Logger;

import frames.LoginFrame;

/*
 * This is the main class
 * @author Elissa Skinner
 */
public class Main {
	
	private static Logger logger = Logger.getLogger(Main.class.getName());
	
	/*
	 * initializes the gui
	 * @param args[] standard main parameters
	 * @return void
	 */
	public static void main(String[] args) {
		logger.info("Starting GUI");
		LoginFrame loginPage = new LoginFrame();
		loginPage.setVisible(true);
	}
	
}
