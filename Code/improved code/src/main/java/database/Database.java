package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

/*
 * This class is a singleton for the database
 * @author Elissa Skinner
 * @author Bryce Johnson
 * @author Jackson O'Donnell
 */
public class Database {
	
	/*
	 * Logger for this file.
	 */
	private static Logger logger = Logger.getLogger(Database.class.getName());
	
	/*
	 * Statically assigns the database driver.
	 */
	private static final String DB_DRIVER
					= "org.apache.derby.jdbc.EmbeddedDriver";
	
	/*
	 * Statically assigns the database to connect to.
	 */
	private static final String DB_CONNECTION = "jdbc:derby:./suedeDB;";
	
	/*
	 * The Connection to the database.
	 */
	private static Connection connection = null;
	
	/*
	 * The Statement to read execute SQL commands.
	 */
	private static Statement statement = null;
	
	static {
		logger.info("connecting to data base");
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		try {
			connection = DriverManager.getConnection(DB_CONNECTION, 
					"", "");
		} catch (SQLException e) {
			logger.severe(e.getMessage());
		}
	}
	
	/*
	 * This method allows communication with the database by way
	 * of specified SQL statements. This method is specifically for
	 * updating the database and will not return a {@link ResultSet}.
	 * 
	 * @param instructions an SQL command to be run
	 * @return True if the SQL command runs properly, False otherwise
	 */
	public static Boolean executeUpdate(String instructions) {
		if (connection != null) {
			try {
				statement = connection.createStatement();
				statement.executeUpdate(instructions);
				return true;
			} catch (SQLException e) {
				logger.severe(e.getMessage());
			}
		} else {
			logger.severe("no database connected");
		}
		return false;
	}
	
	/*
	 * This method allows communication with the database by way
	 * of specified SQL statements. This method is specifically for
	 * retrieving information from the database and subsequently
	 * returns a {@link ResultSet}
	 * 
	 * @param instructions an SQL command to be run
	 * @return A {@link ResultSet} that contains the requested information,
	 *       or a null value if the SQL statement fails.
	 */
	public static ResultSet executeQuery(String instructions) {
		ResultSet rs = null;
		if (connection != null) {
			try {
				statement = connection.createStatement();
				rs = statement.executeQuery(instructions);
			} catch (SQLException e) {
				logger.severe(e.getMessage());
			}
		} else {
			logger.severe("no database connected");
		}
		
		return rs;
	}
	
	/*
	 * This method allows any SQL statement specified to be run.
	 * This method does not allow the retrieval of information as it
	 * does not return a {@link ResultSet}.
	 * 
	 * @param instructions an SQL command to be run
	 * @return Returns True if the SQL command executes properly,
	 *       False otherwise.
	 */
	public static Boolean executeInstruction(String instructions) {
		if (connection != null) {
			try {
				statement = connection.createStatement();
				statement.execute(instructions);
				return true;
			} catch (SQLException e) {
				logger.severe(e.getMessage());
			}
		} else {
			logger.severe("no database connected");
		}
		return false;
	}
}
