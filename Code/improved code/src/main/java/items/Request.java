package items;

import java.sql.ResultSet;
import java.sql.SQLException;

import database.Database;
import user.Account;

/*
 * @author Ellissa Skinner
 * @author Bryce Johnson
 */
public class Request {
	
	private Double buyoutCost;
	private Boolean filled;
	private String subject;
	private Integer quantity;
	private String buyer;
	
	public Request(String buyer, String subject, Integer quantity, Double buyoutCost) {
		this.buyer = buyer;
		this.subject = subject;
		this.quantity = quantity;
		this.buyoutCost = buyoutCost;
		this.filled = false;
	}
	
	public Boolean fill(Account user, String game) {
		String check = "SELECT * FROM userinventory WHERE uName = '" + user.getUserName()
				+ "' AND item = '" + subject + "' AND game = '" + game + "'";
		ResultSet rs = Database.executeQuery(check);
		try {
			if(rs.next()) {
				String command1 = "INSERT INTO userinventory VALUES ('" + buyer
					+ "', '" + subject + "', '" + game + "', " + quantity + ")";
				String command2 = "DELETE FROM orderitems WHERE item = '" + subject
					+ "' AND game = '" + game + "' AND uName = '" + buyer + "' AND quantity = " + quantity
					+ " AND price = " + buyoutCost;
				String command3 = "UPDATE userinfo SET dosh = " + (user.getBalance() + buyoutCost)
					+ ", quantity = " + (rs.getInt("quantity") - quantity) 
					+ " WHERE uName = '" +  user.getUserName() + "'";
				user.setBalance((user.getBalance() + buyoutCost));
				Database.executeUpdate(command1);
				Database.executeUpdate(command2);
				Database.executeUpdate(command3);
				filled = true;
			}else {
				filled = false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return filled;
	}
}
