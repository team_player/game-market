package items;

import java.sql.ResultSet;
import java.sql.SQLException;

import database.Database;
import user.Account;
import user.User;

/*
 * @author Bryce Johnson
 * @author Ellissa Skinner
 */
public class Sale {

	private Double buyoutCost;
	private Boolean sold;
	private String subject;
	private Integer quantity;
	private String seller;
	
	public Sale(String seller, String subject, Integer quantity, Double buyoutCost) {
		this.seller = seller;
		this.subject = subject;
		this.quantity = quantity;
		this.buyoutCost = buyoutCost;
		this.sold = false;
	}
	
	public Boolean buyout(Account user, String game) {
		if(user.getBalance() >= (buyoutCost)) {
			String check = "SELECT * FROM userinventory WHERE uName = '" + user.getUserName()
			+ "' AND item = '" + subject + "' AND game = '" + game + "'";
			ResultSet rs = Database.executeQuery(check);
			
			try {
				if(rs.next()) {
					String update = "UPDATE userinventory SET quantity = " + (rs.getInt("quantity") + quantity)
							+ " WHERE uName = '" +  user.getUserName() + "' AND item = '" + subject
							+ "' AND game = '" + game + "'";
					Database.executeUpdate(update);
				}else {
					String addition = "INSERT INTO userinventory VALUES ('" + user.getUserName()
					+ "', '" + user.getUserName() + "', '" + game + "', " + rs.getInt("quantity") + ")";
					Database.executeUpdate(addition);
				}
			} catch (SQLException e) {
			
			}
			String deletion = "DELETE FROM listeditems WHERE item = '" + subject
			+ "' AND game = '" + game + "' AND uName = '" + seller.replace("'", "''") + "' AND quantity = " + quantity
			+ " AND price = " + buyoutCost;
			String balance = "UPDATE userinfo SET dosh = " + (user.getBalance() - buyoutCost)
				+ " WHERE uName = '" +  user.getUserName() + "'";
			user.setBalance((user.getBalance() - buyoutCost));
			Database.executeUpdate(deletion);
			Database.executeUpdate(balance);
			sold = true;
		}else {
			sold = false;
		}
		return sold;
	}
	
}
