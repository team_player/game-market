package items;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

import javax.swing.JComboBox;
import javax.swing.text.JTextComponent;

import database.Database;
import user.CredentialVerificationService;
import user.UserCreationControler;

/*
 * @author Bryce Johnson
 * @author Jackson O'Donnell
 */
public class ItemActionController {

	private static Logger logger = Logger.getLogger(ItemActionController.class.getName());
	
	public void preformTransaction(String game, String item, String buyer, 
			String seller, Double price) {
		ResultSet rs1 = Database.executeQuery("SELECT * FROM userinventory "
				+ "WHERE uName = '" + seller + "' AND item = '" + item 
				+ "' AND game = '" + game + "'");
		ResultSet rs2 = Database.executeQuery("SELECT dosh FROM userinfo "
				+ "WHERE uName = '" + buyer + "' AND item = '" + item
				+ "' AND game = '" + game + "'");
		try {
			if (rs1.next() && rs2.next()) {
				if (rs2.getDouble("dosh") <= price) {
					// remove item from seller
					// remove subtract money from buyer
					// add item to buyer
					// add money to seller
				}
			}
		} catch (SQLException e) {
			logger.info(e.getMessage());
		}
	}
	
	public static void listItem(JComboBox game, JTextComponent item, JTextComponent price) {
		if (CredentialVerificationService.verifyFieldLengthRequirements(item)
				&& CredentialVerificationService.verifyFieldLengthRequirements(price)) {
			// insert into allitems
			String command;
			ResultSet rs = Database.executeQuery("SELECT * FROM allitems WHERE item = '" 
					+ item.getText().replace("'", "''") + "' AND game = '" 
					+ ((String)game.getSelectedItem()).replace("'", "''") + "'");
			try {
				if (!rs.next()) {
					Database.executeUpdate("INSERT INTO allitems VALUES('" 
							+ item.getText().replace("'", "''") + "', '" 
							+ ((String)game.getSelectedItem()).replace("'", "''") 
							+ "')");
				}
			} catch (SQLException e1) {
				logger.info(e1.getMessage());
			}
			
			// check if in userinventory: update count or add
			rs = Database.executeQuery("SELECT quantity FROM userinventory WHERE uName = '" 
					+ UserCreationControler.getAccount().getUserName()
					.replace("'", "''") + "' AND item = '" 
				+ item.getText().replace("'", "''") + "' AND game = '" 
				+ ((String)game.getSelectedItem()).replace("'", "''") 
				+ "'");
			try {
				if (rs != null && rs.next()) {
					int count = rs.getInt("quantity");
					Database.executeUpdate("UPDATE userinventory SET quantity = " + (count+1) 
							+ " WHERE uName = '" 
							+ UserCreationControler.getAccount().getUserName()
							.replace("'", "''") + "' AND item = '" 
							+ item.getText().replace("'", "''") + "' AND game = '" 
							+ ((String)game.getSelectedItem()).replace("'", "''") 
							+ "'");
				} else {
					Database.executeUpdate("INSERT INTO userinventory VALUES('" 
							+ UserCreationControler.getAccount().getUserName()
							.replace("'", "''") + "', '" + item.getText()
									.replace("'", "''") + "', '" 
							+ ((String)game.getSelectedItem())
									.replace("'", "''") + "', 1)");
				}
			} catch (SQLException e) {
				logger.info(e.getMessage());
			}
			
			// check if in listeditems: update count or add
			command = "SELECT quantity FROM listeditems WHERE uName = '" 
					+ UserCreationControler.getAccount().getUserName()
						.replace("'", "''") + "' AND item = '" 
					+ item.getText().replace("'", "''") + "' AND game = '" 
					+ ((String)game.getSelectedItem()).replace("'", "''") + "'";
			rs = Database.executeQuery(command);
			try {
				if (rs != null && rs.next()) {
					int count = rs.getInt("quantity");
					command = "UPDATE listeditems SET quantity = " + (count+1) 
							+ "WHERE uName = '" 
							+ UserCreationControler.getAccount().getUserName()
							.replace("'", "''") + "' AND item = '" 
							+ item.getText().replace("'", "''") + "' AND game = '" 
							+ ((String)game.getSelectedItem()).replace("'", "''") 
							+ "'";
					Database.executeUpdate(command);
				} else {
					command = "INSERT INTO listeditems VALUES('" + item.getText().replace("'", "''") 
							+ "', '" + ((String)game.getSelectedItem()).replace("'", "''") + "', 1, '"
							+ UserCreationControler.getAccount().getUserName()
							.replace("'", "''") + "', " + Float.parseFloat(price.getText()) + ")";
					Database.executeUpdate(command);
				}
			} catch (SQLException e) {
				logger.info(e.getMessage());
			}
		}
	}
	
	public static void bidItem(JComboBox game, JTextComponent item, JTextComponent price) {
		if (CredentialVerificationService.verifyFieldLengthRequirements(item)
				&& CredentialVerificationService.verifyFieldLengthRequirements(price)) {
			ResultSet rs = Database.executeQuery("SELECT quantity FROM orderitems WHERE uName = '" 
					+ UserCreationControler.getAccount().getUserName()
					.replace("'", "''") + "' AND item = '" 
					+ item.getText().replace("'", "''") + "' AND game = '" 
					+ ((String)game.getSelectedItem()).replace("'", "''") + "'");
			try {
				if (rs != null && rs.next()) {
					int count = rs.getInt("quantity");
					Database.executeUpdate("UPDATE orderitems SET quantity = " + (count+1) 
							+ " WHERE uName = '" 
							+ UserCreationControler.getAccount().getUserName()
							.replace("'", "''") + "' AND item = '" 
							+ item.getText().replace("'", "''") + "' AND game = '" 
							+ ((String)game.getSelectedItem()).replace("'", "''") 
							+ "'");
				} else {
					Database.executeUpdate("INSERT INTO orderitems VALUES('" 
							+ item.getText().replace("'", "''") 
							+ "', '" + ((String)game.getSelectedItem())
								.replace("'", "''") + "', 1, '"
							+ UserCreationControler.getAccount().getUserName()
							.replace("'", "''") + "', " 
							+ Float.parseFloat(price.getText()) + ")");
				}
			} catch (SQLException e) {
				logger.info(e.getMessage());
			}
		}
	}
	
}
