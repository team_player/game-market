package frames;

import java.awt.Color;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.util.logging.Logger;

import javax.swing.JFrame;

import tools.Button;
import tools.Label;
import tools.PasswordField;
import tools.TextField;
import user.UserCreationControler;

/*
 * This is a subclass of Frame. It creates the login frame and 
 * features.
 * @author Elissa Skinner
 */
public class LoginFrame extends Frame {
	
	private static Logger logger = Logger.getLogger
			(LoginFrame.class.getName());
	
	private static final long serialVersionUID = 501L;
	
	private static UserCreationControler ucc = new UserCreationControler();
	
	private Integer position_x = 200;
	private Integer position_y = 120;
	private Integer size_x = 550;
	private Integer size_y = 450;
	private LayoutManager layout = null;
	
	private Color background = new Color(38, 57, 76);
	private final Color WHITE = new Color(255, 255, 255);
	private final Color BLACK = new Color(0, 0, 0);
	
	private Font labelFont = new Font("Britannic Bold", Font.PLAIN, 21);
	private Font buttonFont = new Font("Britannic Bold", Font.PLAIN, 18);
	private Font fieldFont = new Font("Arial", Font.PLAIN, 18);
	private Font errorFont = new Font("Lucida Calligraphy", Font.PLAIN, 15);
	
	private Integer leftBound = size_x * 2/11;
	private Integer topBound = size_y * 2/9;
	private Integer verticalDif = 5;
	private Integer labelHeight = 17;
	private Integer textFieldHeight = 29;
	private Integer buttonHeight = 26;
	
	private Label lblUsername;
	private Label lblPassword;
	private Label lblError;
	
	private Button btnLogin;
	private Button btnCreateAccount;

	private TextField usernameField;
	private PasswordField passwordField;

	/*
	 * Creates a SWING frame for the account creation interface
	 */
	public LoginFrame() {
		logger.info("Creating LoginFrame");
		createFrame();
		createLabels();
		createButtons();
		createTextFields();
		logger.info("LoginFrame successfully created");
	}
	
	/*
	 * (non-Javadoc)
	 * @see frames.Frame#createFrame()
	 */
	protected void createFrame() {
		logger.info("Creating frame");
		setBounds(position_x, position_y, size_x, size_y);
		getContentPane().setBackground(background);
		getContentPane().setLayout(layout);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		logger.info("frame successfully created");
	}
	
	/*
	 * (non-Javadoc)
	 * @see frames.Frame#createLabels()
	 */
	protected void createLabels() {
		logger.info("Creating labels");
		
		lblUsername = new Label("USERNAME", leftBound, topBound, 200, 
				labelHeight, WHITE, labelFont);
		getContentPane().add(lblUsername);
		
		lblPassword = new Label("PASSWORD", leftBound, 
				topBound + labelHeight + verticalDif * 4 + textFieldHeight, 
				200, labelHeight, WHITE, labelFont);
		getContentPane().add(lblPassword);
		
		lblError = new Label(UserCreationControler.getErrorMessage(), 
				leftBound, size_y - 100, 250, labelHeight, WHITE, errorFont);
		getContentPane().add(lblError);
		
		logger.info("Labels successfully created");
	}
	
	/*
	 * (non-Javadoc)
	 * @see frames.Frame#createButtons()
	 */
	protected void createButtons() {
		logger.info("Creating buttons");
		
		btnLogin = new Button("Log-in", leftBound, topBound + labelHeight * 2 
				+ textFieldHeight * 2 + verticalDif * 8, 83, buttonHeight, 
				BLACK, buttonFont);
		btnLogin.addActionListener(this);
		getContentPane().add(btnLogin);
		
		btnCreateAccount = new Button("Create Account", leftBound, topBound 
				+ labelHeight * 2 + textFieldHeight * 2 + buttonHeight 
				+ verticalDif * 11, 156, buttonHeight, BLACK, buttonFont);
		btnCreateAccount.addActionListener(this);
		getContentPane().add(btnCreateAccount);
		
		logger.info("buttons successfully created");
	}
	
	/*
	 * (non-Javadoc)
	 * @see frames.Frame#createTextFields()
	 */
	protected void createTextFields() {
		logger.info("Creating text fields");
		usernameField = new TextField(leftBound, topBound + labelHeight 
				+ verticalDif, size_x - leftBound * 2, textFieldHeight, BLACK, 
				fieldFont);
		getContentPane().add(usernameField);
		
		passwordField = new PasswordField(leftBound, topBound + labelHeight * 2
				+ textFieldHeight + verticalDif * 5, size_x - leftBound * 2, 
				textFieldHeight, BLACK, fieldFont);
		getContentPane().add(passwordField);
		logger.info("Text fields successfully created");
	}

	/*
	 * (non-Javadoc)
	 * @see frames.Frame#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == "Log-in") {
			logger.info("preforming log-in action");
			if (ucc.logIn(usernameField.getText(), passwordField)) {
				dispose();
				HomeFrame home = new HomeFrame();
				home.setVisible(true);
			} else {
				lblError.setText(UserCreationControler.getErrorMessage());
				logger.info("failed to login");
			}
		} else if (e.getActionCommand() == "Create Account") {
			logger.info("preforming create account action");
			UserCreationControler.setErrorMessage("");
			dispose();
			CreateAccountFrame createAccount = new CreateAccountFrame();
			createAccount.setVisible(true);
		} else {
			logger.info("no action preformed");
		}
	}
	
}
