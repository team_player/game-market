package frames;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Logger;

import javax.swing.JFrame;

/*
 * This is an abstract class that extends JFrame.
 * @author Elissa Skinner
 */
public abstract class Frame extends JFrame implements ActionListener {
	
	private static Logger logger = Logger.getLogger(Frame.class.getName());
	
	private static final long serialVersionUID = 50L;
	
	/*
	 * Creates a SWING GUI frame with default values
	 */
	protected void createFrame() {
		logger.info("Creating frame");
		
		setBounds(100, 100, 650, 450);
		getContentPane().setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		logger.info("frame successfully created");
	}
	
	/*
	 * Logs the creation of labels for the SWING interface
	 */
	protected void createLabels() {
		logger.info("Creating labels");
		
		logger.info("labels successfully created");
	}
	
	/*
	 * Logs the creation of buttons for the SWING interface
	 */
	protected void createButtons() {
		logger.info("Creating buttons");

		logger.info("buttons successfully created");
	}
	
	/*
	 * Logs the creation of text fields for the SWING interface
	 */
	protected void createTextFields() {
		logger.info("Creating text fields");

		logger.info("text fields successfully created");
	}
	/*
	 * Logs a failed action event
	 *
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		logger.info("no action preformed");
	}
	
	/*
	 * (non-Javadoc)
	 * @see javax.swing.JFrame#remove(java.awt.Component)
	 */
	@Override
	public void remove(Component comp) {
		super.remove(comp);
	}
	
}
