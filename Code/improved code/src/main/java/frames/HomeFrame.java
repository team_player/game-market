package frames;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import panels.AddItemPanel;
import panels.BidBuyoutPopupPanel;
import panels.EditGamesPanel;
import panels.GamesListPanel;
import panels.InventoryListPanel;
import panels.ItemsListPanel;
import panels.OrderListPanel;
import panels.Panel;
import panels.ProfilePanel;
import panels.SellPopupPanel;
import tools.Button;
import user.UserCreationControler;

/*
 * This is a subclass of Frame. It creates the home frame and 
 * features.
 * @author Elissa Skinner
 * @author Jackson O'Donnell
 * @author Bryce Johnson
 */
public class HomeFrame extends Frame implements MouseListener {
	
	private static Logger logger = Logger.getLogger(HomeFrame.class.getName());
	
	private static final long serialVersionUID = 503L;

	private static UserCreationControler ucc = new UserCreationControler();
	
	private Integer position_x = 100;
	private Integer position_y = 20;
	private Integer size_x = 750;
	private Integer size_y = 650;
	private LayoutManager layout = null;
	
	private Color background = new Color(13, 20, 36);
	private Color panelBackground = new Color(61, 61, 61);
	private Color button_1 = new Color(142, 93, 27);
	private Color button_2 = new Color(66, 39, 9);
	private Color button_3 = new Color(182, 133, 57);
	private final Color WHITE = new Color(255, 255, 255);
	
	private Font font_1 = new Font("Baskerville Old Face", Font.PLAIN, 15);
	private Font font_2 = new Font("Baskerville Old Face", Font.PLAIN, 13);
			
	private Button btnBuy;
	private Button btnSell;
	private Button btnInventory;
	private Button btnAddItem;
	private Button btnProfile;
	private Button btnLogOut;
	private Button eyeOfTheCamel;
	
	private AddItemPanel addItemPanel;
	private BidBuyoutPopupPanel bidbuyoutPopupPanel;
	private SellPopupPanel sellPopupPanel;
	private GamesListPanel gamesListPanel;
	private ItemsListPanel itemsListPanel;
	private OrderListPanel sellGamesPanel;
	private ItemsListPanel sellItemsPanel;
	private InventoryListPanel inventoryGamesPanel;
	private ItemsListPanel inventoryItemsPanel;
	private ProfilePanel profilePanel;
	private EditGamesPanel editGamesPanel;
	private Panel currentPanel = null;
	
	private int height = 23;
	
	private int x_buy = 85;
	private int x_sell = 85;
	private int x_inventory = 85;
	private int x_addRemove = 103;
	private int x_profile = 70;
	private int x_logOut = 78;
	
	private String title_buy = "Buy";
	private String title_sell = "Sell";
	private String title_inventory = "Inventory";
	private String title_addRemove = "Add Item";
	private String title_profile = "Profile";
	private String title_logOut = "Log Out";
	
	private Image camel = null;
	
	/*
	 * Creates the home page Frame. This is the main interface
	 * for the application.
	 */
	public HomeFrame() {
		logger.info("Creating HomeFrame");
		
		createFrame();
		createPanels();
		createButtons();
		
		logger.info("HomeFrame successfully created");
	}
	
	/*
	 * (non-Javadoc)
	 * @see frames.Frame#createFrame()
	 */
	protected void createFrame() {
		logger.info("Creating frame");
		
		setBounds(position_x, position_y, size_x, size_y);
		setDefaultCloseOperation(Frame.EXIT_ON_CLOSE);
		getContentPane().setLayout(layout);
		
		try {
			camel = ImageIO.read(new File("camel.png"));
			this.setContentPane(new JPanel(null) {
				@Override public void paintComponent(Graphics g) {
					super.paintComponent(g);
					g.drawImage(camel, 520, 50, null);
					getContentPane().setBackground(background);
				}
			});
		} catch (IOException e) {
			logger.severe(e.getMessage());
		}
		
		logger.info("frame successfully created");
	}
	
	/*
	 * Creates the parts (panels) within the main interface of
	 * the application.
	 * 
	 * @return void
	 */
	protected void createPanels() {
		logger.info("Creating panels");
		
		itemsListPanel = new ItemsListPanel(this, 305, 27, 300, 650, panelBackground);
		gamesListPanel = new GamesListPanel(0, 27, 300, 650, panelBackground, itemsListPanel);
		
		sellItemsPanel = new ItemsListPanel(this, 305, 27, 300, 650, panelBackground);
		sellGamesPanel  = new OrderListPanel(0, 27, 300, 650, panelBackground, sellItemsPanel);
		
		inventoryItemsPanel = new ItemsListPanel(this, 305, 27, 300, 650, panelBackground);
		inventoryGamesPanel  = new InventoryListPanel(0, 27, 300, 650, panelBackground, inventoryItemsPanel);
		
		
		addItemPanel = new AddItemPanel(x_buy + x_sell, 23, 250, 100, panelBackground);

		profilePanel = new ProfilePanel(0, 23, 500, 650, panelBackground);
		
		bidbuyoutPopupPanel = new BidBuyoutPopupPanel(this, 0, 23, 750, 650, new Color(0, 0, 0, 255));
		sellPopupPanel = new SellPopupPanel(this, 0, 23, 750, 650, new Color(0, 0, 0, 255));
		
		editGamesPanel = new EditGamesPanel(170, 110, 370, 100, panelBackground);
		
		logger.info("panels successfully created");
	}
	
	/*
	 * Creates the buttons within the main interface of
	 * the application.
	 * 
	 * @return void
	 */
	protected void createButtons() {
		logger.info("Creating buttons");
	
		int x_value = 0;
		
		// buy item button
		btnBuy = new Button(title_buy, x_value, 0, x_buy, 28, WHITE, font_1);
		btnBuy.setBackground(button_1);
		btnBuy.addActionListener(this);
		btnBuy.addMouseListener(this);
		getContentPane().add(btnBuy);
		x_value += x_buy;
		
		// sell items button
		btnSell = new Button(title_sell, x_value, 0, x_sell, height, WHITE, font_2);
		btnSell.setBackground(button_2);
		btnSell.addActionListener(this);
		btnSell.addMouseListener(this);
		getContentPane().add(btnSell);
		x_value += x_sell;
		
		// inventory button
		btnInventory = new Button(title_inventory, x_value, 0, x_inventory, height, WHITE, font_2);
		btnInventory.setBackground(button_2);
		btnInventory.addActionListener(this);
		btnInventory.addMouseListener(this);
		getContentPane().add(btnInventory);
		x_value += x_inventory;
		
		// add item button
		btnAddItem = new Button(title_addRemove, x_value, 0, x_addRemove, height, WHITE, font_2);
		btnAddItem.setBackground(button_2);
		btnAddItem.addActionListener(this);
		btnAddItem.addMouseListener(this);
		getContentPane().add(btnAddItem);
		x_value += x_addRemove;
		
		// profile button
		btnProfile = new Button(title_profile, x_value, 0, x_profile, height, WHITE, font_2);
		btnProfile.setBackground(button_2);
		btnProfile.addActionListener(this);
		btnProfile.addMouseListener(this);
		getContentPane().add(btnProfile);
		x_value += x_profile;
		
		// logout button
		btnLogOut = new Button(title_logOut, x_value, 0, x_logOut, height, WHITE, font_2);
		btnLogOut.setBackground(button_2);
		btnLogOut.addActionListener(this);
		btnLogOut.addMouseListener(this);
		getContentPane().add(btnLogOut);
		x_value += x_logOut;
		
		if (UserCreationControler.getAccount().getUserName().equals("Admin")) {
			// eye of the camel
			eyeOfTheCamel = new Button("", 541, 94, 15, 15, WHITE, font_2);
			eyeOfTheCamel.setBackground(button_2);
			eyeOfTheCamel.addActionListener(this);
			eyeOfTheCamel.setOpaque(false);
			getContentPane().add(eyeOfTheCamel);
		}
		
		logger.info("Buttons successfully created");
	}

	/*
	 * Mandatory for ActionListener
	 * Implements actions for components with an action listener
	 * 
	 * @param e event which triggered action listener
	 * @return void
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals(title_buy)) {
			if (currentPanel == gamesListPanel && currentPanel.isVisible()) {
				logger.info("closing " + e.getActionCommand() + " panels");
				remove(gamesListPanel);
				remove(itemsListPanel);
				currentPanel = null;
			} else {
				if (currentPanel != null) {
					if (!currentPanel.isVisible()) {
						currentPanel.setVisible(true);
					}
					if (currentPanel == inventoryGamesPanel) {
						remove(inventoryItemsPanel);
					} else if (currentPanel == sellGamesPanel) {
						remove(sellItemsPanel);
					} 
					logger.info("closing panel");
					remove(currentPanel);
				}
				logger.info("opening " + e.getActionCommand() + " panels");
				currentPanel = gamesListPanel;
				gamesListPanel.update();
				add(gamesListPanel);
				add(itemsListPanel);
			}
		} else if (e.getActionCommand().equals(title_sell)) {
			if (currentPanel == sellGamesPanel && currentPanel.isVisible()) {
				logger.info("closing " + e.getActionCommand() + " panels");
				remove(sellGamesPanel);
				remove(sellItemsPanel);
				currentPanel = null;
			} else {
				if (currentPanel != null) {
					if (!currentPanel.isVisible()) {
						currentPanel.setVisible(true);
					}
					if (currentPanel == gamesListPanel) {
						remove(itemsListPanel);
					} else if (currentPanel == inventoryGamesPanel) {
						remove(inventoryItemsPanel);
					} 
					logger.info("closing panel");
					remove(currentPanel);
				}
				logger.info("opening " + e.getActionCommand() + " panels");
				currentPanel = sellGamesPanel;
				sellGamesPanel.update();
				add(sellGamesPanel);
				add(sellItemsPanel);
			}
		} else if (e.getActionCommand().equals(title_inventory)) {
			if (currentPanel == inventoryGamesPanel && currentPanel.isVisible()) {
				logger.info("closing " + e.getActionCommand() + " panels");
				remove(inventoryGamesPanel);
				remove(inventoryItemsPanel);
				currentPanel = null;
			} else {
				if (currentPanel != null) {
					if (!currentPanel.isVisible()) {
						currentPanel.setVisible(true);
					}
					if (currentPanel == gamesListPanel) {
						remove(itemsListPanel);
					} else if (currentPanel == sellGamesPanel) {
						remove(sellItemsPanel);
					} 
					logger.info("closing panel");
					remove(currentPanel);
					currentPanel = null;
				}
				logger.info("opening " + e.getActionCommand() + " panels");
				currentPanel = inventoryGamesPanel;
				inventoryGamesPanel.update();
				add(inventoryGamesPanel);
				add(inventoryItemsPanel);
			}
		} else if (e.getActionCommand().equals(title_addRemove)) {
			if (currentPanel == addItemPanel && currentPanel.isVisible()) {
				logger.info("closing " + e.getActionCommand() + " panels");
				remove(currentPanel);
				currentPanel = null;
			} else {
				if (currentPanel != null) {
					if (!currentPanel.isVisible()) {
						currentPanel.setVisible(true);
					}
					if (currentPanel == gamesListPanel) {
						remove(itemsListPanel);
					} else if (currentPanel == sellGamesPanel) {
						remove(sellItemsPanel);
					} else if (currentPanel == inventoryGamesPanel) {
						remove(inventoryItemsPanel);
					}
					logger.info("closing panel");
					remove(currentPanel);
				}
				logger.info("opening " + e.getActionCommand() + " panels");
				currentPanel = addItemPanel;
				getContentPane().add(currentPanel);
				currentPanel.setVisible(true);
			}
		} else if (e.getActionCommand().equals(title_profile)) {
			if (currentPanel == profilePanel && currentPanel.isVisible()) {
				logger.info("closing " + e.getActionCommand() + " panels");
				remove(currentPanel);
				currentPanel = null;
			} else {
				if (currentPanel != null) {
					if (!currentPanel.isVisible()) {
						currentPanel.setVisible(true);
					}
					if (currentPanel == gamesListPanel) {
						remove(itemsListPanel);
					} else if (currentPanel == sellGamesPanel) {
						remove(sellItemsPanel);
					} else if (currentPanel == inventoryGamesPanel) {
						remove(inventoryItemsPanel);
					}
					logger.info("closing panel");
					remove(currentPanel);
				}
				logger.info("opening " + e.getActionCommand() + " panels");
				currentPanel = profilePanel;
				add(currentPanel);
			}
		} else if (e.getActionCommand().equals("")) {
			if (currentPanel == editGamesPanel && currentPanel.isVisible()) {
				logger.info("closing " + e.getActionCommand() + " panels");
				remove(currentPanel);
				currentPanel = null;
			} else {
				if (currentPanel != null) {
					if (!currentPanel.isVisible()) {
						currentPanel.setVisible(true);
					}
					if (currentPanel == gamesListPanel) {
						remove(itemsListPanel);
					} else if (currentPanel == sellGamesPanel) {
						remove(sellItemsPanel);
					} else if (currentPanel == inventoryGamesPanel) {
						remove(inventoryItemsPanel);
					}
					logger.info("closing panel");
					remove(currentPanel);
				}
				logger.info("opening " + e.getActionCommand() + " panels");
				currentPanel = editGamesPanel;
				add(currentPanel);
			}
		} else if (e.getActionCommand().equals(title_logOut)) {
			logger.info("preforming log-out action");
			ucc.logOut();
			dispose();
			LoginFrame loginFrame = new LoginFrame();
			loginFrame.setVisible(true);
		} else {
			logger.info("no action preformed");
		}
		
		repaint();
		validate();
	}

	/*
	 * Mandatory for MouseListener
	 * not implemented
	 */
	public void mouseClicked(MouseEvent e) { }

	/*
	 * Mandatory for MouseListener
	 * not implemented
	 */
	public void mousePressed(MouseEvent e) { }

	/*
	 * Mandatory for MouseListener
	 * not implemented
	 */
	public void mouseReleased(MouseEvent e) { }

	/*
	 * Mandatory for MouseListener
	 * changes the color of the component when the mouse is over it
	 * 
	 * @param e event which triggered event listener
	 * @return void
	 */
	public void mouseEntered(MouseEvent e) {
		if (e.getComponent() == btnBuy) {
			e.getComponent().setBackground(button_3);
		} else {
			e.getComponent().setBackground(button_1);
		}
	}

	/*
	 * Mandatory for MouseListener
	 * changes the color of the component when the mouse leaves it
	 * 
	 * @param e event which triggered event listener
	 * @return void
	 */
	public void mouseExited(MouseEvent e) {
		if (e.getComponent() == btnBuy) {
			e.getComponent().setBackground(button_1);
		} else {
			e.getComponent().setBackground(button_2);
		}
	}
	
	public void openPopup(String game, String item, String seller) {
		if (currentPanel == gamesListPanel) {
			logger.info("opening popup");
			
			remove(itemsListPanel);
			remove(gamesListPanel);
			currentPanel = bidbuyoutPopupPanel;
			bidbuyoutPopupPanel.setData(game, item, seller);

		  	add(bidbuyoutPopupPanel);
			validate();
			repaint();
		}
		else if (currentPanel == sellGamesPanel) {
			logger.info("opening popup");
			
			remove(sellGamesPanel);
			remove(sellItemsPanel);
			currentPanel = sellPopupPanel;
			sellPopupPanel.setData(game, item, seller);

		  	add(sellPopupPanel);
			validate();
			repaint();
		}
	}
	
	public void closePopup() {
		if (currentPanel == bidbuyoutPopupPanel) {
			logger.info("closing popup");
			
			remove(bidbuyoutPopupPanel);
			
			currentPanel = gamesListPanel;
			
			add(itemsListPanel);
			add(gamesListPanel);
			
			validate();
			repaint();
		}
		else if (currentPanel == sellPopupPanel) {
			logger.info("closing popup");
			
			remove(sellPopupPanel);
			
			currentPanel = sellGamesPanel;
			
			add(sellGamesPanel);
			add(sellItemsPanel);
			
			validate();
			repaint();
		}
	}
	
	
	
}
