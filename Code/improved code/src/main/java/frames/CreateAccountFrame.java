package frames;

import java.awt.Color;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.logging.Logger;

import tools.Button;
import tools.Label;
import tools.PasswordField;
import tools.TextField;
import user.UserCreationControler;

/*
 * This is a subclass of Frame. It creates the create account frame and 
 * features.
 * @author Elissa Skinner
 */
public class CreateAccountFrame extends Frame {
	
	/*
	 * The logger for this file.
	 */
	private static Logger logger = Logger.getLogger
			(CreateAccountFrame.class.getName());
	
	/*
	 * The serial ID.
	 */
	private static final long serialVersionUID = 502L;

	/*
	 * A controller to add account creation functionality.
	 */
	private static UserCreationControler ucc = new UserCreationControler();
	
	/*
	 * The x position of the frame.
	 */
	private Integer position_x = 200;
	/*
	 * The y position of the frame.
	 */
	private Integer position_y = 120;
	/*
	 * The width of the frame.
	 */
	private Integer size_x = 550;
	/*
	 * The height of the frame.
	 */
	private Integer size_y = 450;
	/*
	 * The layout of the frame.
	 */
	private LayoutManager layout = null;
	
	/*
	 * The background Colour for the frame.
	 */
	private Color background = new Color(38, 57, 76);
	
	/*
	 * Sets a colour to white for ease of use.
	 */
	private final Color WHITE = new Color(255, 255, 255);
	/*
	 * Sets a colour to black for ease of use.
	 */
	private final Color BLACK = new Color(0, 0, 0);

	/*
	 * Sets the font for text fields.
	 */
	private Font textFieldFont = new Font("Arial", Font.PLAIN, 18);
	/*
	 * Sets the font for text.
	 */
	private Font textFont = new Font("Britannic Bold", Font.PLAIN, 20);
	/*
	 * Sets the font for the title.
	 */
	private Font titleFont = new Font("Britannic Bold", Font.PLAIN, 27);
	/*
	 * Sets the font for the back.
	 */
	private Font backFont = new Font("Arial", Font.ITALIC, 10);
	/*
	 * Sets the font for the buttons.
	 */
	private Font buttonFont = new Font("Britannic Bold", Font.PLAIN, 18);
	/*
	 * Sets the font for the error messages.
	 */
	private Font errorFont = new Font("Lucida Calligraphy", Font.PLAIN, 15);

	private Integer verticalDif = 39;
	private Integer height =  29;
	private Integer textWidth = 160;
	private Integer textLeft = size_x / 11;
	private Integer fieldLeft = textLeft + textWidth;
	private Integer topBound = size_y / 5;
	private Integer fieldWidth = size_x - (textLeft * 2) - textWidth;
	
	/*
	 * The {@link TextField} for entering the first name.
	 */
	private TextField firstNameField;
	/*
	 * The {@link TextField} for entering the last name.
	 */
	private TextField lastNameField;
	/*
	 * The {@link TextField} for entering the username.
	 */
	private TextField usernameField;
	/*
	 * The {@link TextField} for entering the password.
	 */
	private PasswordField passwordField;
	/*
	 * The {@link TextField} for re-entering/verifying the password.
	 */
	private PasswordField passwordVerifField;
	
	private Label createAccount;
	private Label firstNameLabel;
	private Label lastNameLabel;
	private Label usernameLabel;
	private Label passwordLabel;
	private Label passwordVerifLabel;
	private Label haveAccountLabel;
	private Label errorLabel;
	
	private Button btnCreateAccount;
	private Button btnLogin;
	
	/*
	 * Creates a SWING frame for the account creation interface
	 */
	public CreateAccountFrame() {
		logger.info("Creating CreateAccountFrame");
		createFrame();
		createTextFields();
		createLabels();
		createButtons();
		logger.info("CreateAccountFrame successfully created");
	}
	
	/*
	 * (non-Javadoc)
	 * @see frames.Frame#createFrame()
	 */
	protected void createFrame() {
		logger.info("Creating frame");
		
		setBounds(position_x, position_y, size_x, size_y);
		getContentPane().setBackground(background);
		setDefaultCloseOperation(Frame.EXIT_ON_CLOSE);
		getContentPane().setLayout(layout);
		
		logger.info("frame successfully created");
	}
	
	/*
	 * (non-Javadoc)
	 * @see frames.Frame#createTextFields()
	 */
	protected void createTextFields() {
		logger.info("Creating text fields");

		// first name text field
		firstNameField = new TextField(fieldLeft, topBound, fieldWidth, 
				height, BLACK, textFieldFont);
		getContentPane().add(firstNameField);

		// last name text field
		lastNameField = new TextField(fieldLeft, topBound + verticalDif, 
				fieldWidth, height, BLACK, textFieldFont);
		getContentPane().add(lastNameField);

		// username text field
		usernameField = new TextField(fieldLeft, topBound + verticalDif * 2,
				fieldWidth, height, BLACK, textFieldFont);
		getContentPane().add(usernameField);

		// password text field
		passwordField = new PasswordField(fieldLeft, topBound + 
				verticalDif * 3, fieldWidth, height, BLACK, 
				textFieldFont);
		getContentPane().add(passwordField);

		// password verification field
		passwordVerifField = new PasswordField(fieldLeft, topBound + 
				verticalDif * 4, fieldWidth, height, BLACK, 
				textFieldFont);
		getContentPane().add(passwordVerifField);
		
		logger.info("Text fields successfully created");
	}
	
	/*
	 * (non-Javadoc)
	 * @see frames.Frame#createLabels()
	 */
	protected void createLabels() {
		logger.info("Creating labels");

		// Create Account title
		createAccount = new Label("Create Account", (size_x - textWidth) / 2,
				topBound / 3, 250, height, WHITE, titleFont);
		getContentPane().add(createAccount);

		// First name label
		firstNameLabel = new Label("First Name:", textLeft, topBound, 
				textWidth, height, WHITE, textFont);
		getContentPane().add(firstNameLabel);

		// Last name label
		lastNameLabel = new Label("Last Name:", textLeft, topBound + 
				verticalDif, textWidth, height, WHITE, textFont);
		getContentPane().add(lastNameLabel);

		// username label
		usernameLabel = new Label("username:", textLeft, topBound + 
				verticalDif * 2, textWidth, height, WHITE, textFont);
		getContentPane().add(usernameLabel);

		// password label
		passwordLabel = new Label("password:", textLeft, topBound + 
				verticalDif * 3, textWidth, height, WHITE, textFont);
		getContentPane().add(passwordLabel);

		// password verification label
		passwordVerifLabel = new Label("verify password:", textLeft, topBound +
				verticalDif * 4, textWidth, height, WHITE, textFont);
		getContentPane().add(passwordVerifLabel);

		// error label
		errorLabel = new Label(UserCreationControler.getErrorMessage(), 
				textLeft, topBound + verticalDif * 6 + 15, 400, height, WHITE, errorFont);
		getContentPane().add(errorLabel);
		
		// already have account label
		haveAccountLabel = new Label("Already have an account?", 
				(size_x - 125 - 66) / 2, topBound + verticalDif * 7 + 15, 125, 
				15, WHITE, backFont);
		getContentPane().add(haveAccountLabel);
		
		logger.info("Labels successfully created");
	}
	
	/*
	 * (non-Javadoc)
	 * @see frames.Frame#createButtons()
	 */
	protected void createButtons() {
		logger.info("Creating buttons");
		
		// submit button
		btnCreateAccount = new Button("submit", (size_x - 90) / 2, 
				topBound + verticalDif * 5 + 10, 90, height, BLACK, buttonFont);
		btnCreateAccount.addActionListener(this);
		getContentPane().add(btnCreateAccount);
		
		// login page button
		btnLogin = new Button("Sign in", (size_x - 125 - 66) / 2 + 125, 
				topBound + verticalDif * 7 + 15, 70, 15, BLACK, backFont);
		btnLogin.addActionListener(this);
		getContentPane().add(btnLogin);
		
		logger.info("Buttons successfully created");
	}
	
	/*
	 * (non-Javadoc)
	 * @see frames.Frame#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == "submit") {
			logger.info("preforming log-in action");
			if (Arrays.equals(passwordField.getPassword(), 
					passwordVerifField.getPassword())) {
				if (ucc.createUser(firstNameField, lastNameField, usernameField, 
						passwordField)) {
					logger.info("successfully created account");
					dispose();
					HomeFrame home = new HomeFrame();
					home.setVisible(true);
				} else {
					errorLabel.setText(UserCreationControler.getErrorMessage());
					logger.info("failed to create account");
				}
			} else {
				UserCreationControler.setErrorMessage("Passwords do not match");
				errorLabel.setText(UserCreationControler.getErrorMessage());
				logger.info("failed to create account. "
						+ "Passwords do not match");
				passwordField.setText("");
				passwordVerifField.setText("");
			}
		} else if (e.getActionCommand() == "Sign in") {
			logger.info("returning to login screen");
			UserCreationControler.setErrorMessage("");
			dispose();
			LoginFrame login = new LoginFrame();
			login.setVisible(true);
		} else {
			logger.info("no action preformed");
		}
	}
	
}
