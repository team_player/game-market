package user;

/*
 * This is the controller for modifying games in the system
 * @author Elissa Skinner
 */
import tools.ComboBox;

import java.util.logging.Logger;

import javax.swing.text.JTextComponent;

import database.Database;
public class AdminActionController {

	private static Logger logger = Logger.getLogger(AdminActionController.class.getName());
	
	/*
	 * adds a game to the database
	 * 
	 * @param game game to be added to database
	 * @return void
	 */
	public static void addGame(JTextComponent game) {
		if (CredentialVerificationService.verifyFieldLengthRequirements(game)) {
			logger.info("adding game " + game.getText());
			Database.executeUpdate("INSERT INTO allgames VALUES('" + game.getText().replace("'", "''") + "')");
		}
	}
	
	/*
	 * removes a game from the database
	 * 
	 * @param bxRemove game to be removed from database
	 * @return void
	 */
	public static void removeGame(ComboBox bxRemove) {
		Database.executeInstruction("DELETE FROM userinventory WHERE game = '"
				+ ((String)bxRemove.getSelectedItem())
					.replace("'", "''") + "'");
		Database.executeInstruction("DELETE FROM allitems WHERE game = '"
				+ ((String)bxRemove.getSelectedItem())
					.replace("'", "''") + "'");
		Database.executeInstruction("DELETE FROM listeditems WHERE game = '" 
				+ ((String)bxRemove.getSelectedItem())
					.replace("'", "''") + "'");
		Database.executeInstruction("DELETE FROM orderitems WHERE game = '"
				+ ((String)bxRemove.getSelectedItem())
					.replace("'", "''") + "'");
		Database.executeInstruction("DELETE FROM allgames WHERE game = '"
				+ ((String)bxRemove.getSelectedItem())
					.replace("'", "''") + "'");
	}
	
	/*
	 * removes item from database
	 * 
	 * @param game game item is from
	 * @param item item to be removed
	 * @return void
	 */
	public static void editGame(ComboBox game, JTextComponent item) {
		Database.executeInstruction("DELETE FROM userinventory WHERE game = '" 
				+ ((String)game.getSelectedItem()).replace("'", "''") 
				+ "' AND item = '" 
				+ ((String)item.getText()).replace("'", "''") + "'");
		Database.executeInstruction("DELETE FROM allitems WHERE game = '" 
				+ ((String)game.getSelectedItem()).replace("'", "''") 
				+ "' AND item = '" 
				+ ((String)item.getText()).replace("'", "''") + "'");
		Database.executeInstruction("DELETE FROM listeditems WHERE game = '" 
				+ ((String)game.getSelectedItem()).replace("'", "''") 
				+ "' AND item = '" 
				+ ((String)item.getText()).replace("'", "''") + "'");
		Database.executeInstruction("DELETE FROM orderitems WHERE game = '" 
				+ ((String)game.getSelectedItem()).replace("'", "''") 
				+ "' AND item = '" 
				+ ((String)item.getText()).replace("'", "''") + "'");
	}
	
}
