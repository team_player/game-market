package user;

/*
 * This class holds the instance of an Account
 * @author Elissa Skinner
 */
public class Account {

	private String firstName;
	private String lastName;
	private String email;
	private String userName;
	private String bio;
	private Double balance;
	
	public Account(String firstName, String lastName, String userName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.bio = "";
		this.balance = 100.00;
	}
	
	public Account(String firstName, String lastName, String userName, 
			String bio, Double dosh) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.userName = userName;
		this.bio = bio;
		this.balance = dosh;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public String getBio() {
		return bio;
	}
	public void setBio(String bio) {
		this.bio = bio;
	}
	
}
