package user;

import java.util.logging.Logger;

import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.text.JTextComponent;

/*
 * This is the controller for editing accounts
 * @author Elissa Skinner
 */
public class UserCreationControler {
	
	private static Logger logger = Logger.getLogger
			(UserCreationControler.class.getName());
	
	private static User currentUser = new User();
	
	private static String errorMessage = "";
	
	/*
	 * verifies data for creating a user
	 * @param fName first name of user
	 * @param lName last name of user
	 * @param username username for account
	 * @param password password for account
	 * @return Boolean
	 */
	public Boolean createUser(JTextComponent fName, JTextComponent lName, 
			JTextComponent username, JPasswordField password) {
		if (CredentialVerificationService.verifyFieldLengthRequirements(fName)
				&& CredentialVerificationService
					.verifyFieldLengthRequirements(lName)
				&& CredentialVerificationService
					.verifyFieldLengthRequirements(username)
				&& CredentialVerificationService
					.verifyFieldLengthRequirements(password)) {
			errorMessage = "";
			return currentUser.createAccount(fName.getText(), lName.getText(), 
					username.getText(), password);
		}
		errorMessage = CredentialVerificationService.getErrorMessage();
		logger.info("invalid data entered in one or more field");
		return false;
	}
	
	/*
	 * verified data for editing account
	 * @param fName first name of user
	 * @param lName last name of user
	 * @param username username for account
	 * @param bio information about user
	 * @return Boolean
	 */
	public Boolean editUser(JTextComponent fName, JTextComponent lName, 
			JLabel username, JTextComponent bio) {
		if (CredentialVerificationService.verifyFieldLengthRequirements(fName)
				&& CredentialVerificationService
					.verifyFieldLengthRequirements(lName)) {
			errorMessage = "";
			return currentUser.editAccount(fName.getText(), lName.getText(),
					username.getText(), bio.getText());
		}
		errorMessage = CredentialVerificationService.getErrorMessage();
		logger.info("invalid data entered in one or more field");
		return false;
	}
	
	/*
	 * tries to retrieve user account with username and password
	 * @param username username for account
	 * @param password password for account
	 * @return Boolean
	 */
	public Boolean logIn(String username, JPasswordField password) {
		Boolean isLoggedin = currentUser.logIn(username, password);
		if (!isLoggedin) {
			errorMessage = CredentialVerificationService.getErrorMessage();
		}
		return isLoggedin;
	}

	/*
	 * deletes user account
	 * @param username specifies account to be deleted
	 * @return void
	 */
	public void deleteUser(String username) {
		currentUser.deleteAccount(username);
	}
	
	/*
	 * logs user out of account
	 * @return void
	 */
	public void logOut() {
		logger.info("logging user out");
		currentUser.logOut();
	}

	/*
	 * returns current account logged into
	 * @return Account
	 */
	public static Account getAccount() {
		return currentUser.getAccount();
	}
	
	/*
	 * returns current error message
	 * @return String
	 */
	public static String getErrorMessage() {
		return errorMessage;
	}

	/*
	 * sets current error message
	 * @param errorMessage current error message
	 * @return void
	 */
	public static void setErrorMessage(String errorMessage) {
		UserCreationControler.errorMessage = errorMessage;
	}
	
}
