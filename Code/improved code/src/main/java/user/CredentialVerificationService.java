package user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

import javax.swing.JPasswordField;
import javax.swing.text.JTextComponent;

import database.Database;

/*
 * This class verifies fields are filled correctly
 * @author Elissa Skinner
 */
public class CredentialVerificationService {
	
	private static Logger logger = Logger.getLogger
			(CredentialVerificationService.class.getName());
	
	private static String errorMessage = "";
	
	/*
	 * verifies that the correct password is entered for the username
	 * @param username specifies account to be accessed
	 * @param password password entered to be verified
	 * @return ResultSet
	 */
	public static ResultSet verifyPassword(String username, JPasswordField password) {
		String command = "SELECT * FROM userinfo WHERE uName = '" 
				+ username + "'";
		ResultSet rs = Database.executeQuery(command);
		try {
			if (rs != null && rs.next()) {
				if (rs.getString("passwd").equals(String.copyValueOf(password.getPassword()))) {
					errorMessage = "";
					return rs;
				}
				errorMessage = "Incorrect password";
				logger.info("incorrect password");
			} else {
				errorMessage = "Incorrect username";
				logger.info("username not found");
			}
		} catch (SQLException e) {
			logger.severe(e.getMessage());
		}
		return null;
	}
	
	/*
	 * verifies component contains text
	 * @param field component to be checked
	 * @return Boolean
	 */
	public static Boolean verifyFilledField(JTextComponent field) {
		if (field.getText().equals("")) {
			errorMessage = "One or more field is empty";
		}
		
		return !field.getText().equals("");
	}
	
	/*
	 * verifies text length is greater than 0 and less than or equal to 25
	 * @param field component to be checked
	 * @return Boolean
	 */
	public static Boolean verifyFieldLengthRequirements(JTextComponent field) {
		if (field.getText().length() > 25) {
			errorMessage = "One or more field exceeds the length requirement";
		} else if (field.getText().length() <= 0) {
			errorMessage = "One or more field is empty";
		}
		
		return field.getText().length() <= 25 && field.getText().length() > 0;
	}
	
	/*
	 * returns the current error message
	 * @return String
	 */
	public static String getErrorMessage() {
		return errorMessage;
	}
	
}