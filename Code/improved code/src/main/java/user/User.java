package user;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

import javax.swing.JPasswordField;

import database.Database;

/*
 * This class executes changes to database for the user account
 * @author Elissa Skinner
 */
public class User {
	
	private static Logger logger = Logger.getLogger(User.class.getName());
	
	private static Account currentAccount;
	
	/*
	 * deletes account from database
	 * @param username specifies unique account to be deleted
	 * @return void
	 */
	public void deleteAccount(String username) {
		String command = "DELETE FROM userinfo WHERE uName = '" + username + "'";
		Database.executeUpdate(command);
	}
	
	/*
	 * creates account in database
	 * @param fName first name of user
	 * @param lName last name of user
	 * @param username username for account
	 * @param password password for account
	 * @return Boolean
	 */
	public Boolean createAccount(String fName, String lName, String username, 
			JPasswordField password) {
		currentAccount = new Account(fName, lName, username);
		String command = "SELECT uName FROM userinfo WHERE uName = '" + username + "'";
		ResultSet rs = Database.executeQuery(command);
		try {
			if (!rs.next()) {
				command = "INSERT INTO userinfo VALUES('" 
						+ fName.replace("'", "''") + "', '" + lName.replace("'", "''") 
						+ "', '" + username.replace("'", "''") + "', '" 
						+ String.copyValueOf(password.getPassword())
						.replace("'", "''") + "', '', 100)";
				return Database.executeUpdate(command);
			}
		} catch (SQLException e) {
			logger.info(e.getMessage());
		}
		UserCreationControler.setErrorMessage("Username has already been used");
		return false;
	}
	
	/*
	 * edits account in database
	 * @param fName first name of user
	 * @param lName last name of user
	 * @param username username for account
	 * @param bio information about user
	 * @return Boolean
	 */
	public Boolean editAccount(String fName, String lName, String username, 
			String bio) {
		String command = "SELECT uName FROM userinfo WHERE uName = '" + username + "'";
		ResultSet rs = Database.executeQuery(command);
		try {
			if (rs.next()) {
				command = "UPDATE userinfo SET fName = '" 
						+ fName.replace("'", "''") + "', lName = '" 
						+ lName.replace("'", "''") + "', bio = '" 
						+ bio.replace("'", "''") + "' WHERE uName  = '"
						+ username.replace("'", "''") + "'";
				Boolean returnVal =  Database.executeUpdate(command);
		
				if (returnVal) {
					currentAccount.setFirstName(fName.replace("''", "'"));
					currentAccount.setLastName(lName.replace("''", "'"));
					currentAccount.setBio(bio.replace("''", "'"));
				}
				
				return returnVal;
			}
		} catch (SQLException e) {
			logger.info(e.getMessage());
		}
		
		return false;
	}
	
	/*
	 * verifies username and password match account in database
	 * @param username username for account
	 * @param password password for account
	 * @return Boolean
	 */
	public Boolean logIn(String username, JPasswordField password) {
		try {
			ResultSet rs = CredentialVerificationService.verifyPassword(username, password);
			if (rs != null) {
				currentAccount = new Account(rs.getString("fName"), 
						rs.getString("lName"), rs.getString("uName"), 
						rs.getString("bio"), rs.getDouble("dosh"));
				return true;
			}
		} catch (SQLException e) {
			logger.severe(e.getMessage());
		}
		return false;
	}
	
	/*
	 * sets current user to null
	 * @return void
	 */
	public void logOut() {
		currentAccount = null;
	}
	
	/*
	 * returns account currently logged into
	 * @return Account
	 */
	public Account getAccount() {
		return currentAccount;
	}
	
}
