package tools;

import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JComboBox;

/*
 * @author Elissa Skinner
 */
public class ComboBox extends JComboBox<Object> {

	private static Logger logger = Logger.getLogger(ComboBox.class.getName());

	private static final long serialVersionUID = 142L;
	
	private List<String> listOfItems = new ArrayList<String>();
	private Integer selectedIndex;
	private Boolean isEditable;
	
	public ComboBox (String[] items, Integer ndx, Boolean isEditable) {
		logger.info("Creating combo box set to " + items[ndx]);
		
		for (Object item : items) {
			addItem(item);
		}
		setSelectedIndex(ndx);
		setEditable(isEditable);
	}
	
	public ComboBox (List<String> items, Integer ndx, Boolean isEditable) {
		logger.info("Creating combo box");
		
		for (Object item : items) {
			addItem(item);
		}
		setSelectedIndex(ndx);
		setEditable(isEditable);
	}
	
	@Override
	public void addItem(Object item) {
		this.listOfItems.add((String)item);
		super.addItem(item);
	}
	
	@Override
	public void setSelectedIndex(int ndx) {
		if (ndx < listOfItems.size()) {
			this.selectedIndex = ndx;
			super.setSelectedIndex(this.selectedIndex);
		}
	}
	
	@Override
	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
		super.setEditable(this.isEditable);
	}
	
}
