package tools;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.util.logging.Logger;

import javax.swing.JPasswordField;

/*
 * @author Elissa Skinner
 */
public class PasswordField extends JPasswordField {

	private static Logger logger = Logger.getLogger(PasswordField.class.getName());

	private static final long serialVersionUID = 144L;
	
	private Integer position_x;
	private Integer position_y;
	private Integer size_x;
	private Integer size_y;
	private Color textColor;
	private Font font;
	
	public PasswordField (Integer posX, Integer posY, Integer sizeX, 
			Integer sizeY, Color color, Font font) {
		logger.info("Creating password field with bounds (" + posX + ", "
				+ posY + ", " + sizeX + ", " + sizeY + "), foreground color "
				+ (color != null ? color.getRGB() : color) + ", and font " 
				+ (font != null ? (font.getFontName() + ", " + font.getStyle()
				+ ", " + font.getSize()) : font));
		
		setBounds(new Rectangle(posX, posY, sizeX, sizeY));
		setForeground(color);
		setFont(font);
	}
	
	@Override
	public void setBounds(Rectangle r) {
		this.position_x = r.x;
		this.position_y = r.y;
		this.size_x = r.width;
		this.size_y = r.height;

		super.setBounds(position_x, position_y, size_x, size_y);
	}
	
	@Override
	public void setForeground(Color color) {
		this.textColor = color;
		
		super.setForeground(textColor);
	}
	
	@Override
	public void setFont(Font font) {		
		this.font = font;
		
		super.setFont(this.font);
	}
	
}
