package tools;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.util.logging.Logger;

import javax.swing.JButton;

/*
 * @author Elissa Skinner
 */
public class Button extends JButton{

	private static Logger logger = Logger.getLogger(Button.class.getName());

	private static final long serialVersionUID = 141L;
	
	private String text;
	private Integer position_x;
	private Integer position_y;
	private Integer size_x;
	private Integer size_y;
	private Color foreground;
	private Font font;
	
	public Button (String text, Integer posX, Integer posY, Integer sizeX, 
			Integer sizeY, Color foreground, Font font) {
		logger.info("Creating button with text \"" + text + "\", bounds (" 
				+ posX + ", " + posY + ", " + sizeX + ", " + sizeY 
				+ "), foreground color " 
				+ (foreground != null ? foreground.getRGB() : foreground) 
				+ ", and font " + (font != null ? (font.getFontName() + ", " 
				+ font.getStyle() + ", " + font.getSize()) : font));
		

		setText(text);
		setBounds(posX, posY, sizeX, sizeY);
		setForeground(foreground);
		setFont(font);
	}
	
	@Override
	public void setText(String text) {
		this.text = text;
		super.setText(this.text);
	}
	
	@Override
	public void setBounds(Rectangle r) {
		this.position_x = r.x;
		this.position_y = r.y;
		this.size_x = r.width;
		this.size_y = r.height;
		
		super.setBounds(position_x, position_y, size_x, size_y);
	}
	
	@Override
	public void setForeground(Color color) {
		this.foreground = color;
		super.setForeground(this.foreground);
	}
	
	@Override
	public void setFont(Font font) {
		this.font = font;
		super.setFont(this.font);
	}
	
}
