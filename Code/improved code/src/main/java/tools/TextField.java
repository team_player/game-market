package tools;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.util.logging.Logger;

import javax.swing.JTextField;

/*
 * @author Elissa Skinner
 */
public class TextField extends JTextField {
	
	private static Logger logger = Logger.getLogger(TextField.class.getName());

	private static final long serialVersionUID = 145L;
	
	private Integer position_x = -1;
	private Integer position_y = -1;
	private Integer size_x = -1;
	private Integer size_y = -1;
	private Color textColor = null;
	private Font font = null;
	
	public TextField (Integer posX, Integer posY, Integer sizeX, 
			Integer sizeY, Color color, Font font) {
		logger.info("Creating text field with bounds (" + posX + ", "
				+ posY + ", " + sizeX + ", " + sizeY + "), foreground color "
				+ (color != null ? color.getRGB() : color) + ", and font " 
				+ (font != null ? (font.getFontName() + ", " + font.getStyle()
				+ ", " + font.getSize()) : font));
		
		setBounds(new Rectangle(posX, posY, sizeX, sizeY));
		setForeground(color);
		setFont(font);
	}
	
	@Override
	public void setBounds(Rectangle r) {
		this.position_x = r.x;
		this.position_y = r.y;
		this.size_x = r.width;
		this.size_y = r.height;

		super.setBounds(position_x, position_y, size_x, size_y);
	}
	
	@Override
	public void setForeground(Color color) {
		this.textColor = color;
		
		super.setForeground(textColor);
	}
	
	@Override
	public void setFont(Font font) {		
		this.font = font;
		
		super.setFont(this.font);
	}
	
}
