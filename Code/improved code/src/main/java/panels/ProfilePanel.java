package panels;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import tools.Label;
import user.User;
import user.UserCreationControler;

/*
 * @author Elissa Skinner
 */
public class ProfilePanel extends Panel implements ActionListener, KeyListener {
	
	private static Logger logger = Logger.getLogger(ProfilePanel.class.getName());
	
	private static final long serialVersionUID = 633;

	private JTextField firstNameField;
	private JTextField lastNameField;
	
	private JLabel firstName;
	private JLabel lastName;
	private JLabel username;
	private JLabel balance;
	private JTextArea bio;
	private Label errorLabel;
	
	private JButton btn;
	
	private final Color WHITE = new Color(255, 255, 255);

	private UserCreationControler profile = new UserCreationControler();

	public ProfilePanel(Integer posX, Integer posY, Integer sizeX, Integer sizeY, 
			Color background) {
		logger.info("Creating Profile Panel");
		
		setBounds(posX, posY, sizeX, sizeY);
		setBackground(background);
		
		setLayout(null);
		createLabels();
		printInfo();
		initialButtons();
		
		logger.info("Profile panel created");
	}
	
	private void createFields() {
		// first name text field
		firstNameField = new JTextField(profile.getAccount().getFirstName());
		firstNameField.setFont(new Font("Arial", Font.PLAIN, 18));
		firstNameField.setBounds(208, 90, 270, 29);
		add(firstNameField);
		
		// last name text field
		lastNameField = new JTextField(profile.getAccount().getLastName());
		lastNameField.setFont(new Font("Arial", Font.PLAIN, 18));
		lastNameField.setBounds(208, 129, 270, 29);
		add(lastNameField);
		
		// biography text field
		bio = new JTextArea(profile.getAccount().getBio());
		bio.setBounds(70, 278, 400, 205);
		bio.setFont(new Font("Arial", Font.PLAIN, 17));
		bio.setForeground(new Color(0, 0, 0));
		bio.setLineWrap(true);
		bio.setWrapStyleWord(true);
		bio.setEditable(true);
		bio.setBackground(WHITE);
		bio.addKeyListener(this);
		add(bio);

		// error label
		errorLabel = new Label(UserCreationControler.getErrorMessage(), 
				70, 490, 400, 29, WHITE, 
				new Font("Lucida Calligraphy", Font.PLAIN, 15));
		add(errorLabel);
	}
	
	private void removeFields() {
		remove(firstNameField);
		remove(lastNameField);
		remove(bio);
		remove(errorLabel);
	}
	
	private void createLabels() {
		// Create Account title
		JLabel createAccount = new JLabel("User Profile");
		createAccount.setBounds(179, 35, 142, 20);
		createAccount.setFont(new Font("Britannic Bold", Font.PLAIN, 27));
		createAccount.setForeground(WHITE);
		add(createAccount);
		
		// First name label
		JLabel firstNameLabel = new JLabel("First Name: ");
		firstNameLabel.setBounds(50, 90, 200, 29);
		firstNameLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		firstNameLabel.setForeground(WHITE);
		add(firstNameLabel);
		
		// Last name label
		JLabel lastNameLabel = new JLabel("Last Name:");
		lastNameLabel.setBounds(50, 129, 200, 29);
		lastNameLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		lastNameLabel.setForeground(WHITE);
		add(lastNameLabel);
		
		// username label
		JLabel usernameLabel = new JLabel("username:");
		usernameLabel.setBounds(50, 168, 200, 29);
		usernameLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		usernameLabel.setForeground(WHITE);
		add(usernameLabel);
		
		// email address label
		JLabel balanceLabel = new JLabel("balance:");
		balanceLabel.setBounds(50, 207, 200, 29);
		balanceLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		balanceLabel.setForeground(WHITE);
		add(balanceLabel);
		
		// biography label
		JLabel bioLabel = new JLabel("biography:");
		bioLabel.setBounds(50, 246, 200, 29);
		bioLabel.setFont(new Font("Britannic Bold", Font.PLAIN, 20));
		bioLabel.setForeground(WHITE);
		add(bioLabel);
	}
	
	private void printInfo() {
		firstName = new JLabel(profile.getAccount().getFirstName());
		firstName.setBounds(220, 90, 250, 29);
		firstName.setFont(new Font("Arial", Font.PLAIN, 20));
		firstName.setForeground(WHITE);
		add(firstName);

		lastName = new JLabel(profile.getAccount().getLastName());
		lastName.setBounds(220, 129, 250, 29);
		lastName.setFont(new Font("Arial", Font.PLAIN, 20));
		lastName.setForeground(WHITE);
		add(lastName);

		username = new JLabel(profile.getAccount().getUserName());
		username.setBounds(220, 168, 250, 29);
		username.setFont(new Font("Arial", Font.PLAIN, 20));
		username.setForeground(WHITE);
		add(username);

		balance = new JLabel(String.format("%.2f", profile.getAccount().getBalance()));
		balance.setBounds(220, 207, 270, 29);
		balance.setFont(new Font("Arial", Font.PLAIN, 20));
		balance.setForeground(WHITE);
		add(balance);

		bio = new JTextArea(profile.getAccount().getBio());
		bio.setBounds(70, 278, 400, 245);
		bio.setFont(new Font("Arial", Font.PLAIN, 17));
		bio.setForeground(WHITE);
		bio.setLineWrap(true);
		bio.setWrapStyleWord(true);
		bio.setEditable(false);
		bio.setBackground(new Color(61, 61, 61));
		add(bio);
	}
	
	private void removeText() {
		remove(firstName);
		remove(lastName);
		remove(bio);
	}
	
	private void initialButtons() {
		// edit account button
		btn = new JButton("Edit Account");
		btn.setBounds(183, 530, 134, 29);
		btn.setForeground(new Color(0, 0, 0));
		btn.setFont(new Font("Britannic Bold", Font.PLAIN, 18));
		add(btn);
		btn.addActionListener(this);
	}
	
	private void editButtons() {
		// save changes button
		btn = new JButton("Save Changes");
		btn.setBounds(178, 530, 145, 29);
		btn.setForeground(new Color(0, 0, 0));
		btn.setFont(new Font("Britannic Bold", Font.PLAIN, 18));
		add(btn);
		btn.addActionListener(this);
	}

	public void keyTyped(KeyEvent e) {
		if (e.getSource() == bio) {
			if (bio.getText().length() > 255) {
				bio.setText(bio.getText().substring(0, bio.getText().length()-1));
			}
		}
	}

	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == "Save Changes") {
			logger.info("preforming action update profile");
			if (profile.editUser(firstNameField, lastNameField, username, 
					bio)) {
				remove(btn);
				initialButtons();
				removeFields();
				printInfo();
				
				repaint();
				validate();
			} else {
				errorLabel.setText(UserCreationControler.getErrorMessage());
				logger.info("failed to update profile");
			}
		} else if (e.getActionCommand() == "Edit Account") {
			UserCreationControler.setErrorMessage("");
			remove(btn);
			editButtons();
			removeText();
			createFields();
			
			repaint();
			validate();
		}
	}

}
