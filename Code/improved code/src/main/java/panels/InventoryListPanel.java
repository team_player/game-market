package panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import database.Database;
import user.UserCreationControler;

/*
 * @author Jackson O'Donnell
 */
public class InventoryListPanel extends Panel {
	
	private static Logger logger = Logger.getLogger(OrderListPanel.class.getName());
	
	private static final long serialVersionUID = 631;
	
	private String command = "SELECT DISTINCT game FROM userinventory WHERE uName = '"+UserCreationControler.getAccount().getUserName()+"'";

	private ItemsListPanel itemsList;
	
	private JTable table = null;
	private DefaultTableModel model = null;
	
	private ResultSet rs = Database.executeQuery(command);
	private ResultSet rs2 = Database.executeQuery("SELECT COUNT(DISTINCT game) FROM userinventory WHERE uName = '"+UserCreationControler.getAccount().getUserName()+"'");
	
    String[] columnNames = {"Games"};
	
	public InventoryListPanel(Integer posX, Integer posY, Integer sizeX, Integer sizeY, 
			Color background, ItemsListPanel itemsList) {
		logger.info("Creating games list panel");
		
		setBounds(posX, posY, sizeX, sizeY);
		setBackground(background);
		
		this.itemsList = itemsList;
		
		initializeUI();
		
		logger.info("games list panel successfully created");
    }

    private void initializeUI() {
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(300, 1000));
        model = new DefaultTableModel(columnNames, 0){

            @Override
            public boolean isCellEditable(int row, int column) {
               return false;
            }
        };
        
        try {
			while(rs.next()) {
				String[] entry = new String[1];
				entry[0] = rs.getString("game");
				model.addRow(entry);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        table = new JTable(model);
        table.setAutoCreateRowSorter(true);
        table.setRowHeight(25);
        JScrollPane pane = new JScrollPane(table);
        
        // customize
        table.setBackground(new Color(61, 61, 61));
        table.setGridColor(new Color(81, 81, 81));    
        table.setForeground(new Color(255, 255, 255));
        table.setSelectionBackground(new Color(182, 143, 67));
        table.setSelectionForeground(new Color(255, 255, 255));
        table.getTableHeader().setBackground(new Color(142, 93, 27));
        table.getTableHeader().setForeground(new Color(255, 255, 255));
        table.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 12));
        table.setFont(new Font("Tahoma", Font.PLAIN, 12));
        
        ListSelectionModel cellSelectionModel = table.getSelectionModel();
        cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        cellSelectionModel.addListSelectionListener(new ListSelectionListener() {

			public void valueChanged(ListSelectionEvent e) {
				String selectedData = null;
				
				int selectedRow = table.getSelectedRow();
				int selectedColumn = table.getSelectedColumn();
				selectedData = (String) table.getValueAt(selectedRow, selectedColumn);
				
				itemsList.setItems(selectedData, "userinventory");
			}
        	
        });
        
        // add it
        add(pane, BorderLayout.CENTER);
    }
    
    public void update() {
    	logger.info("updating");
    	table.getSelectionModel().clearSelection();
    	rs = Database.executeQuery(command);
    	int deleteRowCount = model.getRowCount();
    	for(int i = 0; i < deleteRowCount; i++) {
    		model.removeRow(0);
    	}
    	try {
			while(rs.next()) {
				String[] entry = new String[1];
				entry[0] = rs.getString("game");
				model.addRow(entry);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	
}