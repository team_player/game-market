package panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JComboBox;
import javax.swing.JLabel;

import database.Database;
import tools.Button;
import tools.ComboBox;
import tools.Label;
import tools.TextField;
import user.AdminActionController;

/*
 * @author Ellissa Skinner
 */
public class EditGamesPanel extends Panel implements ActionListener {

	private static Logger logger = Logger.getLogger(EditGamesPanel.class.getName());
	
	private static final long serialVersionUID = 645;

	private final Color WHITE = new Color(255, 255, 255);
	private final Color BLACK = new Color(0, 0, 0);
	
	private Font font = new Font("Tahoma", Font.BOLD, 12);
	
	private Label lblAdd;
	private Label lblRemove;
	private Label lblEdit;
	private Label lblItem;
	
	private TextField fldAdd;
	private ComboBox bxRemove;
	private ComboBox bxEdit;
	private TextField fldEdit;
	
	private Button btnAdd;
	private Button btnRemove;
	private Button btnEdit;
	
	private String strAdd = "Add Game";
	private String strRemove = "Remove Game";
	private String strEdit = "Edit Game";
	private String strItem = "Remove Item";
	
	private ResultSet rs = Database.executeQuery("SELECT * FROM allgames");
	private List<String> gamesListing = new ArrayList();
	
	public EditGamesPanel(Integer posX, Integer posY, Integer sizeX, Integer sizeY, 
			Color background) {
		logger.info("Creating edit games panel");
		
		try {
			while(rs.next()) {
				gamesListing.add(rs.getString("game"));
			}
		} catch (SQLException e) {
			logger.info(e.getMessage());
		}
		
		setBounds(posX, posY, sizeX, sizeY);
		setBackground(background);
		
		initializeUI();
	}
	
	private void initializeUI() {
		setLayout(new GridLayout(4, 3));
    	
		lblAdd = new Label(strAdd, 50, 90, 200, 29, WHITE, font);
		lblAdd.setBackground(new Color(61,61,61));
		lblAdd.setOpaque(true);
		add(lblAdd, BorderLayout.CENTER);
		
		fldAdd = new TextField(0, 0, 270, 29, BLACK, font);
		add(fldAdd, BorderLayout.WEST);
		
		btnAdd = new Button(strAdd, 230, 335, 100, 29, WHITE, font);
		btnAdd.setBackground(new Color(61, 61, 61));
		btnAdd.addActionListener(this);
		add(btnAdd, BorderLayout.CENTER);
		
		lblRemove = new Label(strRemove, 50, 90, 200, 29, WHITE, font);
		lblRemove.setBackground(new Color(61,61,61));
		lblRemove.setOpaque(true);
		add(lblRemove, BorderLayout.CENTER);
		
		bxRemove = new ComboBox(gamesListing, 0, false);
		add(bxRemove, BorderLayout.WEST);
		
		btnRemove = new Button(strRemove, 230, 335, 100, 29, WHITE, font);
		btnRemove.setBackground(new Color(61, 61, 61));
		btnRemove.addActionListener(this);
		add(btnRemove, BorderLayout.CENTER);
		
		lblEdit = new Label(strEdit, 50, 90, 200, 29, WHITE, font);
		lblEdit.setBackground(new Color(61,61,61));
		lblEdit.setOpaque(true);
		add(lblEdit, BorderLayout.WEST);
		
		bxEdit = new ComboBox(gamesListing, 0, false);
		add(bxEdit, BorderLayout.WEST);
		
		add(new JLabel(""));
		
		lblItem = new Label(strItem, 50, 90, 200, 29, WHITE, font);
		lblItem.setBackground(new Color(61,61,61));
		lblItem.setOpaque(true);
		add(lblItem, BorderLayout.WEST);
		
		fldEdit = new TextField(0, 0, 270, 29, BLACK, font);
		add(fldEdit, BorderLayout.WEST);
		
		btnEdit = new Button(strEdit, 230, 335, 100, 29, WHITE, font);
		btnEdit.setBackground(new Color(61,61,61));
		btnEdit.addActionListener(this);
		add(btnEdit, BorderLayout.CENTER);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals(strAdd)) {
			AdminActionController.addGame(fldAdd);
			rs = Database.executeQuery("SELECT * FROM allgames");
			try {
				while(rs.next()) {
					gamesListing.add(rs.getString("game"));
				}
			} catch (SQLException ex) {
				logger.info(ex.getMessage());
			}
			bxRemove.addItem(fldAdd.getText());
			bxEdit.addItem(fldAdd.getText());
			fldAdd.setText("");
		} else if (e.getActionCommand().equals(strRemove)) {
			AdminActionController.removeGame(bxRemove);
			rs = Database.executeQuery("SELECT * FROM allgames");
			try {
				while(rs.next()) {
					gamesListing.add(rs.getString("game"));
				}
			} catch (SQLException ex) {
				logger.info(ex.getMessage());
			}
			bxEdit.removeItem(bxRemove.getSelectedItem());
			bxRemove.removeItem(bxRemove.getSelectedItem());
		} else if (e.getActionCommand().equals(strEdit)) {
			AdminActionController.editGame(bxEdit, fldEdit);
		}
		
	}
	
}
