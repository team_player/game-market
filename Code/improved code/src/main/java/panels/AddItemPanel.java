package panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JTextField;

import database.Database;
import items.ItemActionController;
import tools.Button;
import tools.ComboBox;
import tools.Label;
import tools.TextField;

/*
 * @author Ellissa Skinner
 */
public class AddItemPanel extends Panel implements ActionListener, MouseListener {

	private static Logger logger = Logger.getLogger(AddItemPanel.class.getName());
	
	private static final long serialVersionUID = 635;

	private final Color WHITE = new Color(255, 255, 255);
	private final Color BLACK = new Color(0, 0, 0);
	
	private Font font = new Font("Tahoma", Font.BOLD, 12);
	
	private Label gameLabel;
	private Label catLabel;
	private Label priceLabel;
	
	private ComboBox games;

	private TextField items;
	private TextField priceField;
	
	private Button btnList;
	private Button btnBuy;
	
	private ResultSet rs = Database.executeQuery("SELECT * FROM allgames");
	private List<String> gamesListing = new ArrayList();
	
	public AddItemPanel(Integer posX, Integer posY, Integer sizeX, Integer sizeY, 
			Color background) {
		logger.info("Creating add items panel");
		
		try {
			while(rs.next()) {
				gamesListing.add(rs.getString("game"));
			}
		} catch (SQLException e) {
			logger.info(e.getMessage());
		}
		
		setBounds(posX, posY, sizeX, sizeY);
		setBackground(background);
		
		initializeUI();
		
		logger.info("add items panel successfully created");
	}

	private void initializeUI() {
        // grid
    	setLayout(new GridLayout(4, 2));
    	
    	gameLabel = new Label(" Game: ", 50, 90, 200, 29, WHITE, font);
    	gameLabel.setBackground(new Color(61, 61, 61));
    	gameLabel.setOpaque(true);
    	add(gameLabel, BorderLayout.CENTER);
    	
    	games = new ComboBox(gamesListing, 0, false);
		add(games, BorderLayout.WEST);
    	
        // category label
		catLabel = new Label(" Item: ", 50, 90, 200, 29, WHITE, font);
		catLabel.setBackground(new Color(61, 61, 61));
		catLabel.setOpaque(true);
        add(catLabel, BorderLayout.CENTER);
    	// drop down menu
    	items = new TextField(0, 0, 270, 29, BLACK, font); 
        items.setForeground(BLACK);
        add(items, BorderLayout.CENTER);
        
        // labels & fields
        // buyout label
 		priceLabel = new Label(" Price: ", 50, 90, 200, 29, WHITE, font);
 		priceLabel.setBackground(new Color(61, 61, 61));
 		priceLabel.setOpaque(true);
        add(priceLabel, BorderLayout.CENTER);
		// buyout field
		priceField = new TextField(0, 0, 270, 29, BLACK, font);
        add(priceField, BorderLayout.WEST);
         
        //buttons
        // List
		btnList = new Button("List", 230, 335, 90, 29, WHITE, font);
		btnList.setBackground(new Color(0, 0, 126));
		add(btnList, BorderLayout.CENTER);
		btnList.addActionListener(this);
		btnList.addMouseListener(this);
		add(btnList, BorderLayout.CENTER);
		
		// Sell
		btnBuy = new Button("Buy", 230, 335, 90, 29, WHITE, font);
		btnBuy.setBackground(new Color(0, 0, 126));
		btnBuy.addActionListener(this);
		btnBuy.addMouseListener(this);
		add(btnBuy, BorderLayout.CENTER);
    }

	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == "List") {
			ItemActionController.listItem(games, items, priceField);
		} else if (e.getActionCommand() == "Buy") {
			ItemActionController.bidItem(games, items, priceField);
		}
	}

	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseEntered(MouseEvent e) {
		e.getComponent().setBackground(new Color(0, 0, 216));
	}

	public void mouseExited(MouseEvent e) {
		e.getComponent().setBackground(new Color(0, 0, 126));
	}   
	
}
