package panels;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import frames.HomeFrame;
import items.Sale;
import user.UserCreationControler;
import database.Database;

/*
 * @author Jackson O'Donnell
 * @author Bryce Johnson
 * @author Ellissa Skinner
 */
public class BidBuyoutPopupPanel extends Panel implements ActionListener {
	
	private static Logger logger = Logger.getLogger(BidBuyoutPopupPanel.class.getName());
	
	private static final long serialVersionUID = 636;

	private HomeFrame frame;
	
	private Panel buttons = null;
	
	private JTextField name = new JTextField();
	private JTextField quantity = new JTextField();
	private JTextField buyoutField = new JTextField();
	
	private Sale sale;
	private String game;
	
	public BidBuyoutPopupPanel(HomeFrame frame, Integer posX, Integer posY, 
			Integer sizeX, Integer sizeY, Color background) {
		logger.info("Creating items list panel");
		
		this.frame = frame;
		setBounds(posX, posY, sizeX, sizeY);
		setBackground(background);
		
		setBackground(new Color(0, 0, 0, 200));
		setBounds(0, 23, 750, 650);
		
		Panel info = new Panel();
		Panel labels = new Panel();
		Panel fields = new Panel();
		
		info.setLayout(new BoxLayout(info, BoxLayout.X_AXIS));
		labels.setLayout(new BoxLayout(labels, BoxLayout.Y_AXIS));
		fields.setLayout(new BoxLayout(fields, BoxLayout.Y_AXIS));
		
		
		JLabel lName = new JLabel("Name: ", SwingConstants.LEFT);
		JLabel lDescription = new JLabel("Quantity: ", SwingConstants.LEFT);
		JLabel lBuyout = new JLabel("Buyout: ", SwingConstants.LEFT);
		labels.add(lName);
		labels.add(lDescription);
		labels.add(lBuyout);
		
		
		name.setEditable(false);
		quantity.setEditable(false);
		buyoutField.setEditable(false);
		fields.add(name);
		fields.add(quantity);
		fields.add(buyoutField);
		
		
		info.add(labels);
		info.add(fields);
		
		buttons = new Panel();
		
		final JButton buyout = new JButton("Buyout");
		buyout.setBackground(new Color(0, 0, 126));
		buyout.setForeground(new Color(255, 255, 255));
		buyout.setFont(new Font("Baskerville Old Face", Font.PLAIN, 13));
		buyout.addActionListener(this);
		buyout.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	buyout.setBackground(new Color(0, 0, 216));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	buyout.setBackground(new Color(0, 0, 126));
		    }
		});
		final JButton cancel = new JButton("cancel");
		cancel.setBackground(new Color(126, 0, 0));
		cancel.setForeground(new Color(255, 255, 255));
		cancel.setFont(new Font("Tahoma", Font.BOLD, 12));
		cancel.addActionListener(this);
		cancel.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		    	cancel.setBackground(new Color(216, 0, 0));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	cancel.setBackground(new Color(126, 0, 0));
		    }
		});
		
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.X_AXIS));
		buttons.add(buyout);
		buttons.add(cancel);
		repaint();
		
		//Space left for possible image
		
		removeAll();
		setLayout(new GridBagLayout());
		GridBagConstraints constraint = new GridBagConstraints();
		constraint.gridx = 1;
		constraint.gridy = 0;
		constraint.gridwidth = 2;
		add(info, constraint);
		constraint.gridx = 0;
		constraint.gridy = 1;
		constraint.gridwidth = GridBagConstraints.REMAINDER;
		add(buttons, constraint);
		revalidate();
		repaint();
		//Return frame to re-modify it
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == "Buyout") {
			if(sale.buyout(UserCreationControler.getAccount(), game)) {
				this.frame.closePopup();
				logger.info("preforming buyout");
			}else {
				logger.info("failed to buyout sale");
			}
		} else if (e.getActionCommand() == "cancel") {
			logger.info("canceling bid/buyout");
			sale = null;
			game = null;
			this.frame.closePopup();
		}
	}
	
	public void setData(String game, String item, String seller) {
		String command = "SELECT quantity, price FROM listeditems WHERE uName='"+seller+"' AND item='"+item+"' AND game='"+game+"'";
		ResultSet rs = Database.executeQuery(command);

		try {
			rs.next();
			name.setText(item);
			quantity.setText(rs.getString("quantity"));
			buyoutField.setText(rs.getString("price"));
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.game = game;
		try {
			sale = new Sale(seller, item, rs.getInt("quantity"), rs.getDouble("price"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
}
