package panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import database.Database;
import frames.HomeFrame;

/*
 * @author Bryce Johnson
 * @author Jackson O'Donnell
 */
public class ItemsListPanel extends Panel {
	
	private static Logger logger = Logger.getLogger(ItemsListPanel.class.getName());
	
	private static final long serialVersionUID = 632;
	
	private String command;
	
	private DefaultTableModel model;
	
	private ResultSet rs;
	
	private String currGame;


	private ArrayList<String> items = new ArrayList<String>();
    String[] columnNames = {"Items: ", "Seller: ", "Quantity: "};
	
    HomeFrame frame;
    
	public ItemsListPanel(HomeFrame frame, Integer posX, Integer posY, Integer sizeX, Integer sizeY, 
			Color background) {
		logger.info("Creating items list panel");
		
		this.frame = frame;
		
		setBounds(posX, posY, sizeX, sizeY);
		setBackground(background);
		
		initializeUI();
		
		logger.info("items list panel successfully created");
    }

    private void initializeUI() {
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(300, 1000));
        model = new DefaultTableModel(columnNames, 0){

            @Override
            public boolean isCellEditable(int row, int column) {
               return false;
            }
        }; // change 40 to numGames
        JTable table = new JTable(model){
        	@Override
        	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        	Component comp = super.prepareRenderer(renderer, row, column);
        	comp.setBackground(row % 2 == 0 ? new Color(61, 61, 61) : new Color(101, 101, 101));
        	return comp;
        	}
        };
        table.setAutoCreateRowSorter(true);
        table.setRowHeight(25);
        JScrollPane pane = new JScrollPane(table);
        //table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        
        // customize
        //table.setBackground(new Color(61, 61, 61));
        table.setGridColor(new Color(81, 81, 81));    
        table.setForeground(new Color(255, 255, 255));
        table.setSelectionBackground(new Color(182, 143, 67));
        table.setSelectionForeground(new Color(255, 255, 255));
        table.getTableHeader().setBackground(new Color(142, 93, 27));
        table.getTableHeader().setForeground(new Color(255, 255, 255));
        table.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 12));
        table.setFont(new Font("Tahoma", Font.PLAIN, 12));
        
        table.addMouseListener(new MouseAdapter() {
        	public void mouseClicked(MouseEvent e) {
        		if (e.getClickCount() == 2) {
        	    	JTable target = (JTable)e.getSource();
        	      	int row = target.getSelectedRow();
        	      	
        	      	String s[] = new String[2];
        	      	
        	      	for(int i = 0; i < 2; i++) {
        	      		
        	      		s[i] = (String)target.getValueAt(row, i);
            	      	
        	      	}
        	      	
        	      	frame.openPopup(currGame, s[0], s[1]);
        		}
        	}
        });
        add(pane, BorderLayout.CENTER);
    }
    
    public void setItems(String gameName, String argu) {
    	currGame=gameName;
    	command = "SELECT * FROM " + argu + " WHERE game = '" + gameName + "'";
    	rs = Database.executeQuery(command);
    	int deleteRowCount = model.getRowCount();
    	for(int i = 0; i < deleteRowCount; i++) {
    		model.removeRow(0);
    	}
    	try {
			while(rs.next()) {
				String[] entry = new String [3];
				entry[0] = rs.getString("item");
				entry[1] = rs.getString("uName");
				entry[2] = rs.getString("quantity");
				model.addRow(entry);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
}
