package panels;

import java.awt.Color;
import java.util.logging.Logger;

import javax.swing.JPanel;

import frames.Frame;
import frames.LoginFrame;

/*
 * @author Bryce Johnson
 * @author Elissa Skinner
 * @author Jackson O'Donnell
 */
public class Panel extends JPanel {
	
	private static Logger logger = Logger.getLogger(Panel.class.getName());
	
	private static final long serialVersionUID = 63;

	public Panel() {}
	
	public Panel(Integer posX, Integer posY, Integer sizeX, Integer sizeY, 
			Color background) {
		logger.info("Creating panel");
		
		setBounds(posX, posY, sizeX, sizeY);
		setBackground(background);
		
		logger.info("panel successfully created");
	}
	
}
