package group.project;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class LogIn extends JDialog{

	private JTextField username;
	private JPasswordField password;
	private JLabel lUsername;
	private JLabel lPassword;
	private JButton login;
	private JButton canel;
	private boolean flag;
	
	public LogIn(JFrame frame) {
		super(frame, "Login", true);
		
		JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints constraint = new GridBagConstraints();
		
		lUsername = new JLabel("Username: ");
		constraint.gridx = 0;
		constraint.gridy = 0;
		constraint.gridwidth = 1;
		panel.add(lUsername, constraint);
		
		username = new JTextField(20);
		constraint.gridx = 1;
		constraint.gridy = 0;
		constraint.gridwidth = 2;
        panel.add(username, constraint);
 
        lPassword = new JLabel("Password: ");
        constraint.gridx = 0;
        constraint.gridy = 1;
        constraint.gridwidth = 1;
        panel.add(lPassword, constraint);
 
        password = new JPasswordField(20);
        constraint.gridx = 1;
        constraint.gridy = 1;
        constraint.gridwidth = 2;
        panel.add(password, constraint);
        panel.setBorder(new LineBorder(Color.BLACK));
		
		
	}
}
